class UserRegister{
  String? username;
  String? email;
  String? password;
  String? rol;

  UserRegister();

  Map<String, String> toMap(){
    return{
      'username': username!,
      'email': email!,
      'password': password!,
      'rol': rol!
    };
  }

}