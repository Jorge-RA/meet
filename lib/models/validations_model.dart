
class Validations{
  final String myPassword = '';

  static String? isEmpty(String? value){
    if(value == null || value.isEmpty) return 'Este campo es requerido';
    return null;
  }

  static String? email(String? value){
    if(value == null || value.isEmpty) return 'Este campo es requerido';
    if(!value.contains('@')) return 'Ingrese un email válido';
    return null;
  }

  static String? equalPassword(String? psw, String? rpsw){
    if(psw == null || psw.isEmpty || rpsw == null || rpsw.isEmpty) return 'Este campo es requerido';
    if(!(psw == rpsw)) return 'Las contraseñas no coinciden';
    return null;
  }

  
}