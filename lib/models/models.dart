// ignore_for_file: depend_on_referenced_packages

/*rutas o screens */

//*USERS */
export 'package:meet_new/views/screens/users/ajustes_profile_user.dart';
export 'package:meet_new/views/screens/users/profile_user.dart';
export 'package:meet_new/views/screens/users/views_torneos.dart';



//*Manager*/
export 'package:meet_new/views/screens/manager/info_jugador.dart';
export'package:meet_new/views/screens/manager/info_equipo.dart';
export 'package:meet_new/views/screens/manager/edit_equipo.dart';
export 'package:meet_new/views/screens/manager/agregar_jugador.dart';
export 'package:meet_new/views/screens/manager/edit_equipo.dart';
export 'package:meet_new/views/screens/manager/profile_manager.dart';
export 'package:meet_new/views/screens/manager/app_bar.dart';
export 'package:meet_new/views/screens/manager/torneos_manager.dart';
export 'package:meet_new/views/screens/manager/registro_jugador_form.dart';
export 'package:meet_new/views/screens/manager/registro_jugadores.dart';
export 'package:meet_new/views/screens/manager/registro_equipo.dart';


//*Organizador */
export 'package:meet_new/views/screens/organizador/peticiones_del_torneo.dart';
export 'package:meet_new/views/screens/organizador/edit_torneos_screen.dart';
export 'package:meet_new/views/screens/organizador/edit_torneo.dart';
export 'package:meet_new/views/screens/organizador/profile_organizador.dart';
export 'package:meet_new/views/screens/organizador/torneos_iniciados.dart';
export 'package:meet_new/views/screens/organizador/mis_torneos.dart';
export 'package:meet_new/views/screens/auth-logins/login.dart';
export'package:meet_new/views/screens/auth-logins/registro.dart';
export 'package:meet_new/views/screens/auth-logins/select_rol.dart';
export 'package:meet_new/views/screens/organizador/home_organizador.dart'; /*home del organizador */
export 'package:meet_new/views/screens/organizador/torneos.dart';
export 'package:meet_new/views/screens/organizador/create_torneo.dart';
export 'package:meet_new/views/screens/organizador/edit_profile.dart';
export 'package:meet_new/views/screens/organizador/help_center.dart';
export 'package:meet_new/views/screens/organizador/terms_and_conditions.dart';
export 'package:meet_new/views/screens/organizador/sorteo.dart';

//* organizador/tablas */
export 'package:meet_new/views/screens/organizador/tablas/partidos_tablas.dart';
export 'package:meet_new/views/screens/organizador/tablas/posiciones.dart';
export 'package:meet_new/views/screens/organizador/tablas/equipos.dart';

//*organizador/grupos*/
export 'package:meet_new/views/screens/organizador/grupos/sorteo_generado.dart';
export 'package:meet_new/views/screens/organizador/grupos/partidos_grupos.dart';
export 'package:meet_new/views/screens/organizador/grupos/playoff.dart';



//*packages */
export 'package:sizer/sizer.dart';
export 'package:lottie/lottie.dart'; /*imagenes y gits */
export 'dart:io';
export 'package:flutter/cupertino.dart';

//*temas */
export'package:meet_new/theme/theme.dart'; /*tema del app */




