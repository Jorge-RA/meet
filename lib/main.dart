import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';
import 'package:meet_new/views/screens/manager/edit_profile_manager.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Meet',
      theme: Maintheme.lightTheme,

     initialRoute: 'login', // /login
     routes: {

      'login':(_)=> const Login_Screen(), /*login de inicio de sesión */
      'registro':(_)=> const RegitroUser_Screen(),/*pantalla de registro de usarios */
      'rol':(_)=> const Selec_Rol_Screen(), /*seleccion de rol, dependiendo la eleccion le dara un rol y lo llevará a la pantalla correspondiente*/

        /* organizador */
        'homeorganizador':(_)=> const Organizador_Home_Screen(),/*esta pantalla muestra los botones de navegacion como torneos etc */
        'torneos':(_) => const Torneos_Organizador_Screen(),/*home, de organizador, muestra los partidos*/
        'creartorneo':(_) => const Crear_Torneo_Screen(), /*pantalla de crear torneo */
        'mitorneo':(_) => const Mi_Torneo_Screen(),/*informacion de los torneos creados por el organizador, finalizar torneo, ver sorte */
        'torneosiniciados':(_) => const Torneos_Iniciados_Screen(), /*torneos que aun se estan jugando */
        'profile':(_) => const Profile_Organizador_Screen(),/*perfil del organizador */
        'editprofile':(_) => const Edit_Profile_Screen(),/*editar perfil */
        'edittorneo':(_) => const Edit_Torneo_Screen(), /*editar torneo, en está pantalla se veran los torneos editables*/
        'edittorneoscreen':(_) => const Edit_Torneo_Setting_Screen(), /*en esta pantalla se editará el torneo en especifico */
        'centroayuda':(_) => const Centro_De_Ayuda_Screen(),/*pantalla de centro de ayuda */
        'terminosycondiones':(_) => const Terminos_Y_Condiciones_Screen(), /*pantalla de terminos y condiciones */
        'sorteo':(_) => const Sorteo_Screen(), /*pantalla donde se harán o se verán los torneos*/
        'peticiones':(_) => const Peticiones_De_Aceptacion_Screen(), /*pantalla de peticiones, aceptar o no aceptar un club en tu torneo */
        /*organizador/tablas*/
        'partidos' :(_) => const Partidos_Screen(),/*pantalla del tabBar para ver los partidos */
        'posiciones':(_) => const Posiciones_Screen(),/* pantalla del tabBar para ver las posiciones de los equipos, desde el tapBar*/
        'equiposgrupo':(_) => const Equipos_Grupo_Screen(), /*pantalla de tablas de equipos, se ve desde el tapBar*/
        /*organizador/grupos */
        'sorteogenerado':(_) => const Sorteo_Generado_Screen(),/*pantalla de  grupos donde se verá el tapBar que mostrá ,grupos,partidos y PlayOFF*/
        'partidosGrupos':(_) => const Partidos_Grupos_Screen(),/*pantalla de grupos donde se verán los partidos generados*/
         'playoff':(_) => const PlayOff_Screen(),/*pantalla de los play de grupos */

     /*Manager */
      'registroequipo':(_) => const Registro_Equipo_Screen(), /*Registro de equipo */
      'registrojugador' :(_) => const Registro_Jugadores_Screen(), /*Registro de jugadores */
      'registrojugadorform' :(_) => const Registro_Jugador_Form_Screen(), /*está pantalla es un textformfield, donde se registrará al jugador */
      'appbarManager':(_)=> const AppBar_Manager_Screen(), /*en esta pantalla se constuye el appBar de manager */ 
      'listaTorneos':(_) => const Lista_Torneos_Screen(), /*en está pantalla se veran las listas de torneos, con inscripciones abiertas, iniciados etc */
      'profileManager':(_)=>const Profile_Manager_Screen(), /*perfil del manager */
      'editarequipo':(_)=>const Edit_Equipo_ManagerScreen(), /*pantalla de edicion de equipo*/
      'agregarjugador':(_)=>const Editar_Jugador_Manager_Screen(), /*pantalla de edicion del jugador, como eliminar, agregar, y ver informacion*/
      'editprofilemanager':(_)=>const Edit_Profile_Manager_Screen(),/*pantalla para editar la info del manager */
      'infoequipo':(_)=>const Info_Equipo_Screen(), /*se mostará la informacion del equipo*/
      'infojugador':(_)=>const Info_Jugador_Screen(), /*pantalla para mostrar la informacion del jugador */


     /*user*/
      'vertorneos':(_)=>const Ver_Torneos_Screen(), /*pantalla para ver las listas de torneos */
      'perfiluser':(_)=>const PErfil_User_Screen(), /*pantalla del perfil del usuario */
      'ajustesperfil':(_)=>const Ajustes_Profile_User_Screen(), /*pantalla para ajustar el perfil del usuario */
     },  
    );
  }
}


