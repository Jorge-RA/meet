// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Partidos_Screen extends StatelessWidget {
   
  const Partidos_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      body: Column(
        children: [
          //*FECHA DEL TORNEO   
       Padding(
       padding:EdgeInsets.only(top: 10.w,right: 60.w),
       child: Text('04-Mayo-2022',style: Maintheme.fonstApp.subtitle1,), //todo aqui se debe hacer el llamado a la fecha de los partidos
       ), 
          Divider(color: Maintheme.transparents,height: 1.w,),
          const _partidos_etc(), /*llamado a partidos  */
        ],
      )
    );
      }
     );
  }
}


//*PARTIDOS, FECHAS Y ETC
class _partidos_etc extends StatelessWidget {
  const _partidos_etc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return SizedBox( 
      height: 110.w, //! posible error, arreglar
      child: ListView(
        scrollDirection: Axis.vertical,
     children:
      [
//*TABLA DE PARTIDOS #1
       SizedBox(
        height: 15.w,
        width: 60.w,
        child:Padding(
          padding:  EdgeInsets.only(left: 5.w,right: 5.w),
          child: Card(
            elevation: 0,
             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
             color: Maintheme.tertiaryColor,
             child: Row(
              children: [
    //* HORA DEL JUEGO    
                Padding(
                  padding:  EdgeInsets.only(left: 1.w),
                  child: Text('13:15',style: Maintheme.fonstApp.subtitle1,), //todo <== aqui debe hacer un llamado a la hora del juego
                ),

    //* NOMBRE EQUIPO 1
             Padding(
               padding:  EdgeInsets.only(left: 2.w),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('Equipo 1',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada al nombre del team
             ),
    //* PERFIL EQUIPO 1     
            const CircleAvatar(
              child: FadeInImage(
                placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
              image:AssetImage('assets/imgs/backgrounds/Ellips.png') //todo <== aqui debe mostrar la foto de perfil del torneo
              ),
            ),

    //* TEXTO DE VS.
           Padding(
             padding:  const EdgeInsets.all(8.0),
             child: Text('Vs.',style: Maintheme.fonstApp.subtitle1,),
           ),

          
    //* PERFIL EQUIPO 2    
            const CircleAvatar(
              child: FadeInImage(
                placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
              image:AssetImage('assets/imgs/backgrounds/Ellips.png') //todo <== aqui debe mostrar la foto de perfil del torneo
              ),
            ),  

    //* NOMBRE EQUIPO 2
             Padding(
               padding: EdgeInsets.only(left: 2.w),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('Equipo 2',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada al nombre del team
             ),
              ],
             ),
          ),
        ) ,
        ),

 //! BORRAR DESPUES
     //*TABLA DE PARTIDOS #2
       SizedBox(
        height: 15.w,
        width: 60.w,
        child:Padding(
          padding:  EdgeInsets.only(left: 5.w,right: 5.w),
          child: Card(
            elevation: 0,
             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
             color: Maintheme.tertiaryColor,
             child: Row(
              children: [
    //* HORA DEL JUEGO    
                Padding(
                  padding: EdgeInsets.only(left: 1.w),
                  child: Text('13:15',style: Maintheme.fonstApp.subtitle1,), //todo <== aqui debe hacer un llamado a la hora del juego
                ),

    //* NOMBRE EQUIPO 1
             Padding(
               padding: EdgeInsets.only(left: 2.w),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('Equipo 3',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada al nombre del team
             ),
    //* PERFIL EQUIPO 1     
            const CircleAvatar(
              child: FadeInImage(
                placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
              image:AssetImage('assets/imgs/backgrounds/Ellips.png') //todo <== aqui debe mostrar la foto de perfil del torneo
              ),
            ),

    //* TEXTO DE VS.
           Padding(
             padding: const EdgeInsets.all(8.0),
             child: Text('Vs.',style: Maintheme.fonstApp.subtitle1,),
           ),

          
    //* PERFIL EQUIPO 2    
            const CircleAvatar(
              child: FadeInImage(
                placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
              image:AssetImage('assets/imgs/backgrounds/Ellips.png') //todo <== aqui debe mostrar la foto de perfil del torneo
              ),
            ),  

    //* NOMBRE EQUIPO 2
             Padding(
               padding: EdgeInsets.only(left: 2.w),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('Equipo 4',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada al nombre del team
             ),
              ],
             ),
          ),
        ) ,
        ),

     ],
    ),
    );
    
  }
  );
  }
}