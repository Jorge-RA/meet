// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Equipos_Grupo_Screen extends StatelessWidget {
   
  const Equipos_Grupo_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      body: Column(
        children: [
 
       Row(
       children: [
         Padding(
         padding:EdgeInsets.only(top: 10.w,left: 10.w),
         child: Text('Grupos',style: Maintheme.fonstApp.subtitle1,), 
         ),
         Padding(
         padding:EdgeInsets.only(top: 10.w,left: 20.w),
         child: Text('Pts.',style: Maintheme.fonstApp.subtitle1,), 
         ),
           Padding(
         padding:EdgeInsets.only(top: 10.w,left: 10.w),
         child: Text('G',style: Maintheme.fonstApp.subtitle1,), 
         ),
           Padding(
         padding:EdgeInsets.only(top: 10.w,left: 15.w),
         child: Text('P',style: Maintheme.fonstApp.subtitle1,), 
         ),
         Padding(
         padding:EdgeInsets.only(top: 10.w,left: 6.w),
         child: Text('E',style: Maintheme.fonstApp.subtitle1,), 
         ),

       ],
       ),
           Divider(height: 2.w,color: Maintheme.transparents,),
          const _gruposEquipos() /*llamado a los grupos de eequipos */
        ],
      )
    );
    }
     );
  }
}





//* GRUPOS DE EQUIPOS 
class _gruposEquipos extends StatelessWidget {
  const _gruposEquipos({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
       return SizedBox(
         height:  110.w,
         child: ListView(
          children: [
             //*TABLA DE POSICIONES
          SizedBox(
            height: 15.w,
            width: 60.h,
            child: Padding(
              padding: EdgeInsets.only(left: 3.w,right: 3.w,),
              child: Card(
                color: Maintheme.tertiaryColor,
                 shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                 elevation: 0,
                 child: Row(
                  children: [
     //* FOTO DEL EQUIPO 
             const Padding(
               padding: EdgeInsets.all(8.0),
               child: CircleAvatar(
                child: FadeInImage(
                  placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                image:AssetImage('assets/imgs/backgrounds/Ellips.png') //todo <== aqui debe mostrar la foto de perfil del torneo
                ),
            ),
             ),
            
    //* NOMBRE EQUIPO 1
             Padding(
               padding: const EdgeInsets.all(8.0),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('Equipo 1',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada al nombre del team
             ),     
   //* PUNTOS
     Padding(
               padding:  EdgeInsets.only(left: 7.w),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('11',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada a los puntos del team
             ),

   //*G
     Padding(
               padding:  EdgeInsets.only(left: 12.w),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('4',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada a los G del team
             ),  

    //*P
     Padding(
               padding:  EdgeInsets.only(left: 13.w),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('8',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada a los P del team
             ), 

    //*E
     Padding(
               padding:  EdgeInsets.only(left: 6.w),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('6',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada a los P del team
             ),                             
            ],
                 ),
              ),
            ),
          ),
          ]
          ),
       );

      }
     );
  }
}