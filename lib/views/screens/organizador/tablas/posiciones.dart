// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Posiciones_Screen extends StatelessWidget {
   
  const Posiciones_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
      return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      body:Column(
        children: [
      //*TEXTO DE TABLAS DE POSICIONES  
         Padding(
       padding:EdgeInsets.only(top: 10.w,right: 55.w),
       child: Text('Tabla de posiciones',style: Maintheme.fonstApp.subtitle1,), 
       ),
      Divider(color: Maintheme.transparents,height: 1.w,),

          const _posicionesTabla(), /*llamado a posiciones de los equipos */
        ],
      )
    );
      }
      );
  }
}

//*TABLAS DE POSICIONES
class _posicionesTabla extends StatelessWidget {
  const _posicionesTabla({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
      return Sizer(
      builder: (context, orientation, deviceType) {
    return SizedBox(
      height: 110.w,
      child: ListView(
        children: 
        [
  //*TABLA DE POSICIONES
          SizedBox(
            height: 15.w,
            width: 60.h,
            child: Padding(
              padding: EdgeInsets.only(left: 5.w,right: 5.w,),
              child: Card(
                color: Maintheme.tertiaryColor,
                 shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                 elevation: 0,
                 child: Row(
                  children: [
     //* FOTO DEL EQUIPO 
             const Padding(
               padding: EdgeInsets.all(8.0),
               child: CircleAvatar(
                child: FadeInImage(
                  placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                image:AssetImage('assets/imgs/backgrounds/Ellips.png') //todo <== aqui debe mostrar la foto de perfil del torneo
                ),
            ),
             ),
            
    //* NOMBRE EQUIPO 1
             Padding(
               padding: const EdgeInsets.all(8.0),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('Equipo 1',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada al nombre del team
             ),     
                  ],
                 ),
              ),
            ),
          ),   
              
      ]),
    );
  }
);
  }
}