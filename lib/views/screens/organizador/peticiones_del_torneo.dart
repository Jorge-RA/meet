// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Peticiones_De_Aceptacion_Screen extends StatelessWidget {
   
  const Peticiones_De_Aceptacion_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(),
      // ignore: avoid_unnecessary_containers
      body: Container(
        child: Column(
          children:  [
            Padding(
              padding:  EdgeInsets.only(top: 10.w),
              child: Text('Peticiones del torneo',style: Maintheme.fonstApp.headline2,),
            ),
             const _peticionesTorneo(), /*llamado a peticiones de aceptacion o no */
          ],
        ),
      )
    );
    }
    );
  }
}

//* lISTA DE PETICIONES EN EL TORNEO
class _peticionesTorneo extends StatelessWidget {
  const _peticionesTorneo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return SizedBox(
      height:150.w,
      child: 
  //* PETICION  
         ListView(
        children: [
          Padding(
            padding:  EdgeInsets.only(top: 10.w,left: 5.w,right: 5.w),
            child: Card(
              color: Maintheme.tertiaryColor,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
              elevation: 0,
              child:
  //* TAMAÑO DE CONTENEDOR
             SizedBox(
              height: 20.w,
            child: Row(
               children: [
  //*FOTO DE PERFIL      
                Padding(
                  padding: EdgeInsets.only(left: 3.w),
                  child: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
                     radius: 6.w,
                    backgroundColor: Maintheme.unselect2,
                     child: Text('Ftsas'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
                     ),
                ), 
  //* ICONO DE ACEPTAR            
            Padding(
              padding:  EdgeInsets.only(left: 10.w),
              child: SizedBox(
                  height: 10.w,
                  width: 25.w,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Maintheme.acepts
                    ),
                    onPressed: (){}, //todo <== al tocar debe aceptar al equipo en el torneo
                    child:Icon(Icons.check,size: 10.w,)
                  ),
              ),
            ),

   //* ICONO DE NO ACEPTAR     
          Padding(
              padding:  EdgeInsets.only(left: 10.w),
              child: SizedBox(
                  height: 10.w,
                  width: 25.w,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Maintheme.noacepts
                    ),
                    onPressed: (){}, //todo <== al tocar debe NO aceptar al equipo en el torneo
                    child:Icon(Icons.cancel_outlined,size: 10.w,)
                  ),
              ),
            ),  
          ],
        ),
             )
            ),
          ),
          
        ],
      ),
    );
  }
    );
  }
}