// ignore_for_file: camel_case_types, unnecessary_string_escapes

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Sorteo_Screen extends StatelessWidget {
   
  const Sorteo_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(),
      body: Column(
        children: 
        const [
        _crearSorteo() /**llamado a crear torneo */
        ],
      )
    );
  }
  );
  }
}

//* CREAR TORNEO
class _crearSorteo extends StatefulWidget {
  const _crearSorteo({Key? key}) : super(key: key);

  @override
  State<_crearSorteo> createState() => _crearSorteoState();
}

class _crearSorteoState extends State<_crearSorteo> {

  bool _selectCheck = false; /*select de Fase de grupo */
  bool _selecplayOff = false; /*select PlayOffs */
  bool _selectPublicarPlayoff = false; /*select para publicar playoff */
 
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
      return Column(
        children: [

    //* FOTO DE PERFIL DEL TORNEO
        Padding(
          padding:  EdgeInsets.only(top: 15.w),
          child: Center(
            child: ClipRRect( //todo <== Aqui se debe mostrar la foto del campeonatado
              borderRadius: BorderRadius.circular(300),
              child: Image.asset('assets/imgs/backgrounds/Ellips.png',fit: BoxFit.contain,),
            ),
          ),
        ),

   //*FASE DE GRUPO 
        Padding(
      padding: EdgeInsets.only(top: 20.w,left: 10.w),
      child: Row(
      children: [
        Text('Fase de grupo',style: Maintheme.fonstApp.subtitle1,),
        IconButton(
          onPressed: (){mostrarInformacion(context);}, // todo <== debe abir un pop up mostrando informacion 
          icon: Icon(Icons.question_mark_outlined,color: Maintheme.unselect2,size: 4.w,)
          ),

    //*CHECK DE SELECCION
          Padding(
            padding:  EdgeInsets.only(left: 25.w),
            child: IconButton(
              icon: Icon(
                _selectCheck
                ?
                Icons.check_box_outlined
                :
                Icons.check_box_outline_blank_outlined
                ,color: Maintheme.quaternaryColor,), 

              onPressed: () {  //todo <== al tocar se debe activar o desactivar el check y validad la seleccion
                setState(() {
                  _selectCheck = !_selectCheck;
                });
              },
              ),
          ),   
      ],
      ),
    ),
 //* NUMERO DE EQUIPOS
      Padding(
      padding:  EdgeInsets.only(left: 15.w),
      child: Row(
        children: [
          Text('Número de grupos:',style: Maintheme.fonstApp.subtitle1,),
          Padding(
            padding:  EdgeInsets.only(left: 2.w),
            child: Text('10.grupos',style: Maintheme.fonstApp.subtitle1,),
          )
        ],
      ),
      ),

 //*PLAYOFFS      
      //! ME TOCA HACER UN POP UP PARA ESTE
  Padding(
      padding: EdgeInsets.only(top: 10.w,left: 10.w),
      child: Row(
      children: [
        Text('Playoffs',style: Maintheme.fonstApp.subtitle1,),
        IconButton(
          onPressed: (){mostrarInformacion(context);}, // todo <== debe abir un pop up mostrando informacion 
          icon: Icon(Icons.question_mark_outlined,color: Maintheme.unselect2,size: 4.w,)
          ),

          //*CHECK DE SELECCION PLAYOFFS
          Padding(
            padding:  EdgeInsets.only(left: 37.w),
            child: IconButton(
              icon: Icon(
                _selecplayOff
                ?
                Icons.check_box_outlined
                :
                Icons.check_box_outline_blank_outlined
                ,color: Maintheme.quaternaryColor,), 

              onPressed: () {  //todo <== al tocar se debe activar o desactivar el check y validad la seleccion
                setState(() {
                 _selecplayOff = !_selecplayOff;
                });
              },
              ),
          ),   
      ],
      ),
    ),

 //*CUANTOS EQUIPOS CLASIFICAN
        Padding(
      padding:  EdgeInsets.only(left: 15.w),
      child: Row(
        children: [
          Text('Cuantos equipos clasifican:',style: Maintheme.fonstApp.subtitle1,),
          Padding(
            padding:  EdgeInsets.only(left: 2.w),
            child: Text('5.grupos',style: Maintheme.fonstApp.subtitle1,),
          )
        ],
      ),
      ),

//*PUBLICAR LOS PLAYOFF
     Padding(
      padding: EdgeInsets.only(top: 10.w,left: 9.w),
      child: Row(
      children: [
        Text('¿Quieres publicar los playoffs\n\ despues de finalizada la fase de\n\ grupos?',style: Maintheme.fonstApp.subtitle1),
        IconButton(
          onPressed: (){mostrarInformacion(context);}, // todo <== debe abir un pop up mostrando informacion 
          icon: Icon(Icons.question_mark_outlined,color: Maintheme.unselect2,size: 4.w,)
          ),

          //*CHECK DE SELECCION PUBLICAR LOS PLAYOFFS
         
           IconButton(
              icon: Icon(
                _selectPublicarPlayoff
                ?
                Icons.check_box_outlined
                :
                Icons.check_box_outline_blank_outlined
                ,color: Maintheme.quaternaryColor,), 

              onPressed: () {  //todo <== al tocar se debe activar o desactivar el check y validad la seleccion
                setState(() {
                 _selectPublicarPlayoff = !_selectPublicarPlayoff;
                });
              },
        ),          
      ],
      ),
    ),

//* BOTON DE GENERAR SORTEO
    Divider(height: 20.w,color: Maintheme.transparents,),
    SizedBox(
      height: 12.w,
      width: 60.w,
      child: ElevatedButton(
      onPressed: (){// todo <== debe generar el torneo y guardar los cambios
        Navigator.pushNamed(context, 'sorteogenerado');
      }, 
      child: const Text('Generar sorteo')
      ),
    )
    ],
    );

  }
  
  );
  }

   //*CUADRO DE DIALOGO PARA MOSTRAR INFORMACION
    Future<String?> mostrarInformacion(BuildContext context) {
    /**
     * Si el sistema operativo no es un iOS, entonces mostrará está alerta.
     * (Alertas para Android)
     */
    if (!Platform.isIOS) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: Maintheme.tertiaryColor,
          elevation: 0,
          title: Text(
            'Fase de grupo',
            style:Maintheme.fonstApp.headline2,
          ),
          content: Text('Aqui se mostrará la información correspondiente',
              style: Maintheme.fonstApp.subtitle1,),
          actions: <Widget>[
            SizedBox(
              child: Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  Text('Está bien',style: Maintheme.fonstApp.subtitle1,)),
                  
                         
                ]
              ),
            ),
          ],
        ),
      );
    }

    /**
     * Si el sistema operativo es un iOS, entonces mostrará está otra alerta.
     */
    return showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text(
          'Fase de grupo',
          style: Maintheme.fonstApp.headline1,
        ),
        content: Text('Aqui se mostrará la información correspondiente',
            style:Maintheme.fonstApp.headline1,),
        actions: <Widget>[
          CupertinoDialogAction(
            child:  Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  Text('Está bien',style: Maintheme.fonstApp.subtitle1,)),
                  
                          
                ]
              ),
            
          ),
        ],
      ),
    );
  }
}