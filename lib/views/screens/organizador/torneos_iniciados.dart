// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';


class Torneos_Iniciados_Screen extends StatelessWidget {
   
  const Torneos_Iniciados_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
    return const Scaffold(
        
        body: SafeArea(
          child: _TabBar(), /*llamado a TapBar */
      ),
      );
      }
    );
  }
}

//* TAB BAR
class _TabBar extends StatefulWidget {
  const _TabBar({Key? key}) : super(key: key);

  @override
  State<_TabBar> createState() => _TabBarState();
}

class _TabBarState extends State<_TabBar> {
  ScrollController controller = ScrollController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: DefaultTabController(
          length: 3,
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  flexibleSpace: FlexibleSpaceBar(
                     centerTitle: true,
                     title: Text('torneo.name',style: Maintheme.fonstApp.headline2,), //todo <== aqui va el nombre del torneo
                     background: const FadeInImage(
                       height:60,
                       width: 60,
                      fadeInCurve: Curves.easeIn,
                       placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                       image:AssetImage('assets/imgs/backgrounds/Ellips.png') //todo <== aqui debe mostrar la foto de perfil del torneo
                     ),                         
                  ),
                  
                  expandedHeight: 55.w,
                  floating: false,
                  pinned: true,
                  snap: false,
               
                ),
                SliverPersistentHeader(
                  delegate: _SliverAppBarDelegate(
    //*TEXTO DE PARTIDOS, POSICIONES, EQUIPOS
                    TabBar(
                      labelStyle: Maintheme.fonstApp.subtitle1,
                      indicatorColor: Maintheme.quaternaryColor,
                    labelColor: Maintheme.quaternaryColor,
                    unselectedLabelColor: Colors.grey,
                      tabs: const [
                        Tab(child: Text('Partidos',),),
                        Tab(child: Text('Posiciones',),),
                        Tab(child: Text('Equipos',),),
                      ],
                    ),
                  ),
                  pinned: true,
                ),
              ];
            },
            body: const TabBarView(
    //* CONTENEDORES QUE MUESTRA, PARTIDOS, POSICIONES Y EQUIPOS
              children: 
              [
                 Partidos_Screen(), /*Partidos */
                 Posiciones_Screen(), /*Posiciones */
                 Equipos_Grupo_Screen(), /*Equipos, grupos */
            ],
          )
          ),
        ),
      ),
    );
  }
}






//* CONTROLADOR DEL TAPBAR
class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return false;
  }
}








