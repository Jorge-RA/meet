// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Centro_De_Ayuda_Screen extends StatelessWidget {
   
  const Centro_De_Ayuda_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(),
      body: Center(
         child: Text('Centro de ayuda',style: Maintheme.fonstApp.headline2,),
      ),
    );
  }
}