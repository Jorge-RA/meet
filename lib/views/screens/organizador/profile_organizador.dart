
// ignore_for_file: camel_case_types


import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Profile_Organizador_Screen extends StatelessWidget {
   
  const Profile_Organizador_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      body: Column(
        children: 
        const [
          _fotoPerfil()
        ],
      )
    );
      }
     );
  }
}

//* PERFIL Y DATOS DEL USUARIO
class _fotoPerfil extends StatelessWidget {
  const _fotoPerfil({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
   
     //* FOTO DE PERFIL DEL "USUARIO"
     //! Cambiar por un circleavatar y un fade image
         Padding(
           padding:  EdgeInsets.only(top: 10.w),
           child: CircleAvatar(
              backgroundColor: Maintheme.tertiaryColor,
              radius: 20.w,
              child: const FadeInImage(
                  fadeInCurve: Curves.decelerate,
                  placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                image:AssetImage('assets/imgs/backgrounds/perfil.png') //todo <== aqui debe mostrar la foto de perfil del Manager
                 ),
               ),
         ),

    //* NOMBRE DE USUARIO
      Divider(height: 2.w,),
     Text('name.user',style: Maintheme.fonstApp.headline2,),  


    //*AJUSTES DE TORNEO
        Padding(
          padding:  EdgeInsets.only(left: 5.w,top: 5.w),
          child: GestureDetector(
            onTap: (){Navigator.pushNamed(context, 'edittorneo');}, //todo <== lleva a ajustes del torneo
            child: Row(
              children: [
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.settings,color: Maintheme.quaternaryColor,),
                ),
                Text('Ajustes del torneo',style: Maintheme.fonstApp.headline2,),
                Padding(
                   padding:EdgeInsets.only(left: 22.w),
                   child: const Icon(Icons.keyboard_arrow_right_sharp,color: Maintheme.quaternaryColor,),
                 ),
              ],
            ),),
        ),
     
     //*MODIFICAR PERFIL
        Padding(
          padding:  EdgeInsets.only(left: 5.w,top: 5.w),
          child: GestureDetector(
            onTap: (){Navigator.pushNamed(context, 'editprofile');},// todo lleva a modificar perfil
            child: Row(
              children: [
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.mode_edit_outlined,color: Maintheme.quaternaryColor,),
                ),
                Text('Modificar perfil',style: Maintheme.fonstApp.headline2,),
                 Padding(
                   padding:EdgeInsets.only(left: 30.w),
                   child: const Icon(Icons.keyboard_arrow_right_sharp,color: Maintheme.quaternaryColor,),
                 ),
              ],
            ),),
        ),
       
    //*CENTRO DE AYUDA      
          Padding(
          padding:  EdgeInsets.only(left: 5.w,top: 5.w),
          child: GestureDetector(
            onTap: (){Navigator.pushNamed(context, 'centroayuda');}, // todo <== lleva a centro de ayuda
            child: Row(
              children: [
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.help_outline_rounded,color: Maintheme.quaternaryColor,),
                ),
                Text('Centro de ayuda',style: Maintheme.fonstApp.headline2,),
                 Padding(
                   padding:EdgeInsets.only(left: 25.w),
                   child: const Icon(Icons.keyboard_arrow_right_sharp,color: Maintheme.quaternaryColor,),
                 ),
              ],
            ),),
        ),

     //*TERMINOS Y CONDICIONES
       Padding(
          padding:  EdgeInsets.only(left: 5.w,top: 5.w),
          child: GestureDetector(
            onTap: (){Navigator.pushNamed(context, 'terminosycondiones');}, //todo <== lleva a terminos y condiciones
            child: Row(
              children: [
               const Padding(
                 padding: EdgeInsets.all(8.0),
                 child: Icon(Icons.article_outlined,color: Maintheme.quaternaryColor,),
               ),
                Text('Términos y condiciones',style: Maintheme.fonstApp.headline2,),
                 Padding(
                   padding:EdgeInsets.only(left: 10.w),
                   child: const Icon(Icons.keyboard_arrow_right_sharp,color: Maintheme.quaternaryColor,),
                 ),
              ],
            ),),
        ),

     //*CERRAR SESION
           Padding(
          padding:  EdgeInsets.only(left: 5.w,top: 5.w),
          child: GestureDetector(
            onTap: (){cerrarSesion(context);}, //todo <== debe abrir el cuadro de dialogo para cerrar sesion
            child: Row(
              children: [
                const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(Icons.exit_to_app_outlined,color: Maintheme.quaternaryColor,),
                ),
                Text('Cerrar sesión',style: Maintheme.fonstApp.headline2,),
                 Padding(
                   padding:EdgeInsets.only(left: 32.w),
                   child: const Icon(Icons.keyboard_arrow_right_sharp,color: Maintheme.quaternaryColor,),
                 ),
              ],
            ),),
        ),
      ],
    );
  }


   //*CUADRO DE DIALOGO PARA CERRAR SESION
    Future<String?> cerrarSesion(BuildContext context) {
    /**
     * Si el sistema operativo no es un iOS, entonces mostrará está alerta.
     * (Alertas para Android)
     */
    if (!Platform.isIOS) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: Maintheme.tertiaryColor,
          elevation: 0,
          title: Text(
            'Deseas cerrar la sesión?',
            style:Maintheme.fonstApp.headline2,
          ),
          content: Text('Cerrarás la sesión pero podrás ingresar nuevamente',
              style: Maintheme.fonstApp.subtitle1,),
          actions: <Widget>[
            SizedBox(
              child: Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  Text('Cancelar',style: Maintheme.fonstApp.subtitle1,)),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'login', 
                          (route) => false
                        );
                      },
                       child:  Text('Aceptar',style: Maintheme.fonstApp.subtitle1,))        
                ]
              ),
            ),
          ],
        ),
      );
    }

    /**
     * Si el sistema operativo es un iOS, entonces mostrará está otra alerta.
     */
    return showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text(
          'Deseas cerrar la sesión?',
          style: Maintheme.fonstApp.subtitle1,
        ),
        content: Text('Cerrarás la sesión pero podrás ingresar nuevamente',
            style:Maintheme.fonstApp.subtitle1,),
        actions: <Widget>[
          CupertinoDialogAction(
            child:  Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  const Text('Cancelar',style: TextStyle(color: Colors.blue,fontSize: 20))),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'login', 
                          (route) => false
                        );
                      },
                       child:  const Text('Aceptar',style: TextStyle(color: Colors.blue,fontSize: 20)))        
                ]
              ),
            
          ),
        ],
      ),
    );
  }
}