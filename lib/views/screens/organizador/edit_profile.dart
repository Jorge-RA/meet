// ignore_for_file: camel_case_types, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Edit_Profile_Screen extends StatelessWidget {
   
  const Edit_Profile_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
      return Sizer(
      builder: (context, orientation, deviceType) {
        return  Scaffold(
      appBar: AppBar(),
      body: Column(
           children: const [
             _ajustesPerfil() /*llamado a ajustes del perfil */
           ],
      )
    );
      }
    );
  }
}

//** AJUSTES DEL PERFIL 
class _ajustesPerfil extends StatelessWidget {
  const _ajustesPerfil({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
    return Container(
      child: Column(
        children: [
          Form(child: 
          Padding(
            padding: EdgeInsets.only(top: 10.w, left: 5.w,right:5.w),
            child: Column(
              children: 
              [ 


    //* EDITAR NOMBRE
                   TextFormField(
                  autocorrect: false,
                  autofocus: false,
                  style: Maintheme.fonstApp.subtitle1,
                  keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      
                    ),
                 ),
    //*CAMBIAR CORREO
           Divider(height: 5.w,color: Maintheme.transparents,),
          TextFormField(
                  autocorrect: false,
                  autofocus: false,
                  style: Maintheme.fonstApp.subtitle1,
                  keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      hintText: 'Editar correo',
                    
                    ),
                 ),
    //*CAMBIAR CONTRASEÑA
          Divider(height: 5.w,color: Maintheme.transparents,),
          TextFormField(
                  autocorrect: false,
                  autofocus: false,
                  style: Maintheme.fonstApp.subtitle1,
                  keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      hintText: 'Editar contraseña',
                    
                    ),
                 ),

    //* BOTON DE GUARDADO
           Divider(height: 15.w,color: Maintheme.transparents,),
           SizedBox(
            height: 10.w,
            width: 60.w,
            child: ElevatedButton(
              onPressed: (){}, // todo debe guardar los datos
              child: const Text('Guardar')
              ),
            )             
              ],
            ),
          ))
        ],
      ),
    );
    }
    );
  }
}