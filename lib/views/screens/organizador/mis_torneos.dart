// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Mi_Torneo_Screen extends StatelessWidget {
   
  const Mi_Torneo_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(),
      body: Column(
       children:
        const [
         _infoTorneo() /* llamado a  informacion del torneo*/
       ],
      )
    );
    }
    );
  }
}

//* INFORMACION DEL TORNEO
class _infoTorneo extends StatefulWidget {
  const _infoTorneo({Key? key}) : super(key: key);

  @override
  State<_infoTorneo> createState() => _infoTorneoState();
}

class _infoTorneoState extends State<_infoTorneo> {

  bool isSwitched = false; /*si el switch está apagado o encendido */
  

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    // ignore: avoid_unnecessary_containers
    return Container(
      child: Column(
        children: 
        [
    //* FOTO DE PERFIL DEL TORNEO
          Padding(
            padding:  EdgeInsets.only(top: 15.w),
            child: Center(
              child: ClipRRect( //todo <== Aqui se debe mostrar la foto del campeonatado
                borderRadius: BorderRadius.circular(300),
                child: Image.asset('assets/imgs/backgrounds/Ellips.png',fit: BoxFit.contain,),
              ),
            ),
          ),

  //* NOMBRE DEL TORNEO 
      Divider(color: Maintheme.transparents,height: 5.w,),
      Text('Torneo.name',style: Maintheme.fonstApp.headline4,),//todo <== aqui va el nombre del torneo


 //*FINALIZAR INSCRIPCIONES DEL TORNEO
   Padding(
     padding:  EdgeInsets.only(left: 10.w,top: 10.w),
     child: Row(
       children: [
         Text('Finalizar inscripciones',style: Maintheme.fonstApp.subtitle1,),

          Padding(
            padding: EdgeInsets.only(left: 20.w),
            child: Switch.adaptive( //todo <== al tocar se debe cerrar las inscripciones
            activeColor: Maintheme.secondaryColor,
              value: isSwitched, 
              onChanged:(value){
                setState(() {
                  isSwitched = value;
                });
              }
              ),
          ),
       ],
     ),
   ),

//* VER JUGADORES INSCRITOS   
    Padding(
      padding:  EdgeInsets.only(left: 13.w),
      child: Row(
        children: [
          GestureDetector(
            onTap: (){Navigator.pushNamed(context, 'peticiones');}, //Todo <== debe llevar a ver los aquipos inscritos al torneo
            child: Text('Ver inscritos',style: Maintheme.fonstApp.subtitle1,)
          ),
        ],
      ),
    ),

//* SORTEO 
      Padding(
        padding:  EdgeInsets.only(left: 10.w,top: 12.w),
        child: Row(
          children: [
            GestureDetector(onTap:(){Navigator.pushNamed(context, 'sorteo');}, //todo <== debe llevar a ver el sorteo del campeonato
            child: Text('Sorteo',style: Maintheme.fonstApp.subtitle1,)
            ),
            IconButton(
              onPressed: (){Navigator.pushNamed(context, 'sorteo');}, //todo <== debe llevar a ver el sorteo del campeonato
              icon: Padding(
                padding:  EdgeInsets.only(left: 50.w),
                child: const Icon(Icons.arrow_forward_ios,color: Maintheme.quaternaryColor,),
              )
              )
          ],
        ),
      ),

//* EDITAR TORNEO 
    Padding(
        padding:  EdgeInsets.only(left: 10.w,top: 6.w),
        child: Row(
          children: [
            GestureDetector(onTap:(){Navigator.pushNamed(context, 'edittorneo');}, //todo <== debe llevar a editar el campeonato
            child: Text('Editar torneo',style: Maintheme.fonstApp.subtitle1,)
            ),
            IconButton(
              onPressed: (){Navigator.pushNamed(context, 'edittorneo');}, //todo <== debe llevar a editar el campeonato
              icon: Padding(
                padding:  EdgeInsets.only(left: 40.w),
                child: const Icon(Icons.arrow_forward_ios,color: Maintheme.quaternaryColor,),
              )
              )
          ],
        ),
      ),    
    ],
   ),
 );
  }
  );
  }
}