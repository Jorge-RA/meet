// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Edit_Torneo_Screen extends StatelessWidget {
   
  const Edit_Torneo_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(),
      body: Column(
        children: 
        const [
          _editTorneo(), /*llamado a editar torneo */
        ],
      )
    );
      }
    );
  }
}

//*EDITAR TORNEOS
class _editTorneo extends StatelessWidget {
  const _editTorneo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return SizedBox(
       height: 145.w, //!posible error
      child: ListView(
        scrollDirection: Axis.vertical,
        children:
       <Widget>[

  //* MIS TORNEOS TEXTO
        Column(
          children: [
             Text('Editar torneos',style: Maintheme.fonstApp.headline2,),
            Row( /*texto de MIS TORNEOS */
              children: [                
                Padding(
                  padding:  EdgeInsets.all(3.w),
                  child: Text('Mis torneos',style: Maintheme.fonstApp.headline4,),
                ),
                Icon(Icons.circle,color: Maintheme.secondaryColor,size: 4.w,) /*puntos azul */
              ],
            ),
          ],
        ),
  //*TORNEO 1
        Padding(
          padding:  EdgeInsets.only(left: 2.w,right: 2.w),
          child: GestureDetector(
            child: Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
              elevation: 0,
              color: Maintheme.tertiaryColor,
              child: ListTile(
                onTap: (){ /*te lleva a la editar del torneo */
                 Navigator.pushNamed(context, 'edittorneoscreen'); //todo <== lleva a editar torneo
                },
                title: Row(
                  children: [
                    Text('Torneo 1',style: Maintheme.fonstApp.subtitle1,),
                    Padding(
                      padding:  EdgeInsets.only(left:40.w ),
                      child: Icon(Icons.edit,size: 8.w,),
                    )
                  ],
                ), //todo <== aqui debe ir el nombre delo de los torneos, creados por el organizador 
                    
                leading: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
                 backgroundColor: Maintheme.unselect2,
                 child: Text('Ftsas'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
                ),
              ),
            ),
          ),
        ),

        

    //* TORNEOS INICIADOS TEXTOS       
         Column(
          children: [
            Row( /*texto de MIS TORNEOS */
              children: [                
                Padding(
                  padding:  EdgeInsets.all(3.w),
                  child: Text('Torneos iniciados',style: Maintheme.fonstApp.headline4,),
                ),
                Icon(Icons.circle,color: Colors.green.shade300,size: 4.w,)
              ],
            ),
          ],
        ),
    //*TORNEOS 2 
        Padding(
          padding:  EdgeInsets.only(left: 2.w,right: 2.w),
          child: GestureDetector(
            child: Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
              elevation: 0,
              color: Maintheme.tertiaryColor,
              child: ListTile(
                onTap: (){ /*te lleva a la editar del torneo */
                 Navigator.pushNamed(context, 'edittorneoscreen'); //todo <== lleva a editar torneo
                },
                title: Row(
                  children: [
                    Text('Torneo 1',style: Maintheme.fonstApp.subtitle1,),
                    Padding(
                      padding:  EdgeInsets.only(left:40.w ),
                      child: Icon(Icons.edit,size: 8.w,),
                    )
                  ],
                ), //todo <== aqui debe ir el nombre delo de los torneos, creados por el organizador 
                    
                leading: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
                 backgroundColor: Maintheme.unselect2,
                 child: Text('Ftsas'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
                ),
              ),
            ),
          ),
        ),

   //*TORNEOS 2  
        Padding(
          padding:  EdgeInsets.only(left: 2.w,right: 2.w),
          child: GestureDetector(
            child: Card(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
              elevation: 0,
              color: Maintheme.tertiaryColor,
              child: ListTile(
                onTap: (){ /*te lleva a la editar del torneo */
                 Navigator.pushNamed(context, 'edittorneoscreen'); //todo <== lleva a editar torneo
                },
                title: Row(
                  children: [
                    Text('Torneo 1',style: Maintheme.fonstApp.subtitle1,),
                    Padding(
                      padding:  EdgeInsets.only(left:40.w ),
                      child: Icon(Icons.edit,size: 8.w,),
                    )
                  ],
                ), //todo <== aqui debe ir el nombre delo de los torneos, creados por el organizador 
                    
                leading: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
                 backgroundColor: Maintheme.unselect2,
                 child: Text('Ftsas'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
                ),
              ),
            ),
          ),
        ),

    
      ],
    ),
    );
      }
     );
  }
}