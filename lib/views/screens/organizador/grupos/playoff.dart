// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class PlayOff_Screen extends StatelessWidget {
   
  const PlayOff_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
    //* BOTON DE PUBLICAR PARTIDOS       
          Divider(height: 4.w,),
          SizedBox(
            height: 10.w,
            width: 50.w,
            child: ElevatedButton(
              onPressed: (){}, // todo <== debe enviar y publicar los partidos
              child: const Text('Publicar')             
            ),
          ),
          Divider(height: 5.w,color: Maintheme.transparents,),
          const _playOff() /* llamado a los play offs */
        ],
      )
    );
  }
}

//* PLAY OFF
class _playOff extends StatelessWidget {
  const _playOff({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
        return SizedBox(
            height: 90,
            child: ListView(
        children: 
        [
 //* NUMERO DE GRUPOS
      Padding(
        padding: EdgeInsets.only(left: 10.w),// todo <== aqui se debe llamar el texto de los grupos que haya
        child: Text('Grupo 1',style:Maintheme.fonstApp.subtitle1 ,),
      ),

  //*GRUPOS DE PLAYOFFS
          SizedBox(
            height: 15.w,
            width: 60.h,
            child: Padding(
              padding: EdgeInsets.only(left: 5.w,right: 5.w,),
              child: Card(
                color: Maintheme.tertiaryColor,
                 shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                 elevation: 0,
                 child: Row(
                  children: [
     //* FOTO DEL EQUIPO 
             const Padding(
               padding: EdgeInsets.all(8.0),
               child: CircleAvatar(
                child: FadeInImage(
                  placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                image:AssetImage('assets/imgs/backgrounds/Ellips.png') //todo <== aqui debe mostrar la foto de perfil del torneo
                ),
            ),
             ),
            
    //* NOMBRE EQUIPO 1
             Padding(
               padding: const EdgeInsets.all(8.0),
               //! falta poner que el texto si es muy largo, se divida en 2
               child: Text('Equipo 1',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada al nombre del team
             ),     

                  ],
                 ),
              ),
            ),
          ),   
              
      ]),

        );
      }
     );
  }
}