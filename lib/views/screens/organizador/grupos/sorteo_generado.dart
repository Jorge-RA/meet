// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Sorteo_Generado_Screen extends StatelessWidget {
   
  const Sorteo_Generado_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
    return  const Scaffold(
      body: SafeArea(
        child: _tapBarGrupo(), /*lllamado a tapbar */
        ),
    );
      }
    );
  }
}

//* TapBar de GRUPOS
class _tapBarGrupo extends StatefulWidget {
  const _tapBarGrupo({Key? key}) : super(key: key);

  @override
  State<_tapBarGrupo> createState() => _tapBarGrupoState();
}

class _tapBarGrupoState extends State<_tapBarGrupo> {
  @override
  Widget build(BuildContext context) {
     return SafeArea(
      child: Scaffold(
        body: DefaultTabController(
          length: 3,
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  flexibleSpace: FlexibleSpaceBar(
                     centerTitle: true,
                     title: Text('torneo.name',style: Maintheme.fonstApp.headline2,), //todo <== aqui va el nombre del torneo
                     background: const FadeInImage(
                       height:60,
                       width: 60,
                      fadeInCurve: Curves.easeIn,
                       placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                       image:AssetImage('assets/imgs/backgrounds/Ellips.png') //todo <== aqui debe mostrar la foto de perfil del torneo
                     ),                         
                  ),
                  
                  expandedHeight: 55.w,
                  floating: false,
                  pinned: true,
                  snap: false,
               
                ),
                SliverPersistentHeader(
                  delegate: _SliverAppBarDelegateGrupos(
                    //*TEXTO DE PARTIDOS, POSICIONES, EQUIPOS
                    TabBar(
                      labelStyle: Maintheme.fonstApp.subtitle1,
                      indicatorColor: Maintheme.quaternaryColor,
                    labelColor: Maintheme.quaternaryColor,
                    unselectedLabelColor: Colors.grey,
                      tabs: const [
                        Tab(child: Text('Grupos',),),
                        Tab(child: Text('Partidos',),),
                        Tab(child: Text('Playoffs',),),
                      ],
                    ),
                  ),
                  pinned: true,
                ),
              ];
            },
            body: TabBarView(
              //* CONTENEDORES QUE MUESTRA, PARTIDOS, POSICIONES Y EQUIPOS
              children: 
              [
                 
                Center(child: Text('En construcción...',style: Maintheme.fonstApp.headline2,),),/*Grupos*/
                 const Partidos_Grupos_Screen(), /*Partidos */
                 const PlayOff_Screen() /*Playoff */
            ],
          )
          ),
        ),
      ),
    );
  }
}

//* CONTROLADOR DEL TAPBAR GRUPOS
class _SliverAppBarDelegateGrupos extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegateGrupos(this._tabBar);

  final TabBar _tabBar;

  @override
  double get minExtent => _tabBar.preferredSize.height;

  @override
  double get maxExtent => _tabBar.preferredSize.height;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      child: _tabBar,
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegateGrupos oldDelegate) {
    return false;
  }
}