// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Crear_Torneo_Screen extends StatelessWidget {
   
  const Crear_Torneo_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(),
      body: SizedBox(
        height: double.infinity,
        // width: size.width,
        child: SingleChildScrollView(
          child: Column(
            children: 
            const [
             _creacionTorneo() /*llamado a creacion de torneo */
            ],
          ),
        ),
      )
    );
      }
    );
  }
}


//* CREACION DE TORNEO
class _creacionTorneo extends StatelessWidget {
  const _creacionTorneo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
  return Sizer(
   builder: (context, orientation, deviceType) {
       return SizedBox(  
         height: size.height,
         width:size.width,
         child: SingleChildScrollView(
           child: Column(
            children: [
              Text('Crea tu propio torneo',style: Maintheme.fonstApp.headline2,),
         
    //* CREACION DEL TORNEO
              Divider(color: Maintheme.transparents,height: 10.w,),
              Form(child: 
              Padding(
                padding:  EdgeInsets.only(left: 5.w,right:5.w),
                child: Column(
                 children: [
                     TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(
                        hintText: 'Nombre del torneo',
                      
                      ),
                   ),
         
    //*CIERRE DE INSCRIPCION
                   Divider(color: Maintheme.transparents,height:8.w,),
                   Row(
                     children: [
                       Text('Cierre de inscripciones',style: Maintheme.fonstApp.subtitle1,),
                       IconButton(
                        onPressed: (){}, //todo <== debe abrir el calendario
         
                        icon:  const Icon(Icons.calendar_month_outlined,color: Maintheme.quaternaryColor)
                        )
                     ],
                   ),
         
    //*CALENDARIO DEL TORNEO
                   Row(
                     children: [
                       Text('Calendario del torneo',style: Maintheme.fonstApp.subtitle1,),
                       IconButton(
                        onPressed: (){}, //todo <== debe abrir el calendario
                        icon:  const Icon(Icons.calendar_month_outlined,color: Maintheme.quaternaryColor)
                        )
                     ],
                   ),
    //* DIAS DE JUEGO
                   Divider(height: 5.w, color: Maintheme.transparents,),
                   Row(
                     children: [
                       Text('Dias de juego',style: Maintheme.fonstApp.subtitle1,),
                     ],
                   ),
                   
                   SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                     child: Row(
                       children: [   //todo <== debe seleccionar los dias en los que se juegan los partidos              
                         TextButton(onPressed: (){}, child: Text('L',style: Maintheme.fonstApp.headline4,)), /*lunes */
                         TextButton(onPressed: (){}, child: Text('M',style: Maintheme.fonstApp.headline4)),/*martes */
                         TextButton(onPressed: (){}, child: Text('M',style: Maintheme.fonstApp.headline4)), /*miercoles */
                         TextButton(onPressed: (){}, child: Text('J',style: Maintheme.fonstApp.headline4)), /*jueves */
                         TextButton(onPressed: (){}, child: Text('V',style: Maintheme.fonstApp.headline4)), /*viernes */
                         TextButton(onPressed: (){}, child: Text('S',style: Maintheme.fonstApp.headline4)), /*sabado */
                         TextButton(onPressed: (){}, child: Text('D',style: Maintheme.fonstApp.headline4)), /*domingo */
                       ],
                     ),
                   ),
         
    //*CIUDAD O MUNICIPIO
                    Divider(height: 5.w, color: Maintheme.transparents,),
                    TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(
                        hintText: 'Ciudad o municipio',
                       
                      ),
                   ),
         
    //* ESCENARIOS DEPORTIVOS
                    Divider(height: 10.w, color: Maintheme.transparents,),
                   Row(
                     children: [
                       Text('Escenarios deportivos',style: Maintheme.fonstApp.subtitle1,),
                       IconButton(onPressed: (){}, //todo <== debe agregar un nuevo alert dialogo para darle nombre y agregar un escnario deportivo
                       icon: Icon(Icons.add_circle_outline_outlined,color: Maintheme.quaternaryColor,size: 8.w,))
                     ],
                   ),
                  
                   Row(
                     children: [ // todo <== aqui se van a agregar los escenarios 
                       Text('Cancha 1',style: Maintheme.fonstApp.subtitle1,),
                     ],
                   ),
                   Divider(color: Maintheme.transparents,height: 3.w,),
                   Row(
                     children: [ // todo <== aqui se van a agregar los escenarios 
                       Text('Cancha 2',style: Maintheme.fonstApp.subtitle1,),
                     ],
                   ),
         
   //* REQUISITOS DE INSCRIPCION
             Divider(color: Maintheme.transparents,height: 10.w,),
              TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(
                        hintText: 'Requisitos de inscripción',
                        
                      ),
                   ),
              
         
  //* MECANICA DEL TORNEO
             Divider(color: Maintheme.transparents,height: 10.w,),
              TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(
                        hintText: 'Mecanica del torneo',
                       
                      ),
                   ),
         
  //* PREMIO 
              Divider(color: Maintheme.transparents,height: 10.w,),
              TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(
                        hintText: 'Premio',
                     
                      ),
                   ),

  //* BOTON DE GUARDAR TORNEO       
                    Divider(color: Maintheme.transparents,height: 10.w,),
                   SizedBox(
                    height: 13.w,
                    width: 70.w,
                    child: ElevatedButton(onPressed: (){}, 
                    child: const Text('Guardar')
                  ),
                   ),
                    Divider(color: Maintheme.transparents,height:30.w,),
                 ],
                ),
              )
            )
            ],
           ),
         ),
       );
      }
     );
  }
}