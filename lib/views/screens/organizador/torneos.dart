// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Torneos_Organizador_Screen extends StatelessWidget {
   
  const Torneos_Organizador_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      body: Column(
        children:          
       [   
  //* BOTON DE CREAR TORNEO
        Padding(
          padding:  EdgeInsets.only(left: 70.w),
          child: IconButton( /*boton de crear torneo */
          icon: Icon(Icons.add_circle,color: Maintheme.secondaryColor,size: 13.w,), 
          onPressed: () { //todo <== debe llevar a la pantalla de crear torneo
             Navigator.pushNamed(context, 'creartorneo');
          },
          ),
        ),
          const _misTorneos(),/*llamado a lista de torneos,  */
        
        ],
      )
    );
      }
    );
  }
}


//*MIS TORNEOS
class _misTorneos extends StatelessWidget {
  const _misTorneos({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return SizedBox(
      
     height: 145.w, //!posible error
      child: ListView(
        scrollDirection: Axis.vertical,
        children:
       <Widget>[

  //* MIS TORNEOS
        Column(
          children: [
             Text('Torneos',style: Maintheme.fonstApp.headline2,),
            Row( /*texto de MIS TORNEOS */
              children: [                
                Padding(
                  padding:  EdgeInsets.all(3.w),
                  child: Text('Mis torneos',style: Maintheme.fonstApp.headline4,),
                ),
                Icon(Icons.circle,color: Maintheme.secondaryColor,size: 4.w,) /*puntos azul */
              ],
            ),
          ],
        ),
  //* TORNEOS 1
        Padding(
          padding: EdgeInsets.only(left: 2.w,right: 2.w),
          child: GestureDetector(
            
            child: Card(
               shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
               elevation:0,
              color: Maintheme.tertiaryColor,
              child: ListTile(
                onTap: (){ /*te lleva a la info del torneo */
                  Navigator.pushNamed(context, 'mitorneo');
                },
                title: Text('Torneo 1',style: Maintheme.fonstApp.subtitle1,), //todo <== aqui debe ir el nombre delo de los torneos, creados por el organizador 
                    
                leading: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
                 backgroundColor: Maintheme.unselect2,
                 child: Text('Ftsas'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
                ),
              ),
            ),
          ),
        ),

        

  //* TORNEOS INICIADOS
       
         Column(
          children: [
            Row( /*texto de MIS TORNEOS */
              children: [                
                Padding(
                  padding:  EdgeInsets.all(3.w),
                  child: Text('Torneos iniciados',style: Maintheme.fonstApp.headline4,),
                ),
                Icon(Icons.circle,color: Colors.green.shade300,size: 4.w,)
              ],
            ),
          ],
        ),
    //*TORNEOS
        Padding(
          padding: EdgeInsets.only(left: 2.w,right: 2.w),
          child: Card(
           
             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                  elevation: 0,
                  color: Maintheme.tertiaryColor,
            child: ListTile( /*texto de torneos */
              title: GestureDetector(
                onTap: (){Navigator.pushNamed(context, 'torneosiniciados');},//todo <== debe llevar a los partidos, posiciones, equipos del torneo de ese torneo
                child: Text('Torneo 4',style: Maintheme.fonstApp.subtitle1,)
              ), //todo <== aqui debe ir el nombre delo de los torneos, creados por el organizador 
          
              leading: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
               backgroundColor: Maintheme.unselect2,
               child: Text('TR'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
              ),
            ),
          ),
        ),
    //* TORNEOS
    Divider(height: 2.w,color: Colors.transparent,),
         Padding(
           padding:EdgeInsets.only(left: 2.w,right: 2.w) ,
           child: Card(
           
             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                  elevation: 0,
                  color: Maintheme.tertiaryColor,
             child: ListTile(
              title: GestureDetector(
                onTap: (){Navigator.pushNamed(context, 'torneosiniciados');}, //todo <== debe llevar a los partidos, posiciones, equipos del torneo de ese torneo
                child: Text('Torneo 5',style: Maintheme.fonstApp.subtitle1,)), //todo <== aqui debe ir el nombre delo de los torneos, creados por el organizador 
           
              leading: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
               backgroundColor: Maintheme.unselect2,
               child: Text('PG'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
              ),
                   ),
           ),
         ),
      ],
    ),
    );
     }
     );
  }
}

