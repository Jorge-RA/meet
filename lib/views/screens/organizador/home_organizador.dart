// ignore_for_file: camel_case_types


import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Organizador_Home_Screen extends StatefulWidget {
   
  const Organizador_Home_Screen({Key? key}) : super(key: key);

  @override
  State<Organizador_Home_Screen> createState() => _Organizador_Home_ScreenState();
}

class _Organizador_Home_ScreenState extends State<Organizador_Home_Screen> {

   int pageIndex = 0; /*valor de las paginas */

   final pages = [ /*contenedores de las pagina que se mostrarán en el bottom navegator */

  
    const Page1(), //! falta hacer la Page 1
     const Torneos_Organizador_Screen(), /*partidos */
     const Profile_Organizador_Screen() /*perfil */
   
  ];
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('MEET'),
        
        ),

  //*BOTON DE NAVEGACION
     bottomNavigationBar: BottomNavigationBar(
     selectedItemColor: Maintheme.quaternaryColor, /*cambiar color al seleccionar */
    unselectedItemColor: Maintheme.unselect,/*color del que no está seleccionado */
   showUnselectedLabels: false, /*solo muestra las letras del icon seleccionado */
      currentIndex: pageIndex,
      onTap: (index)=>setState(() =>pageIndex = index, ),
      items: 
      const [
        BottomNavigationBarItem( /*boton de ver partidos */
          icon: Icon(Icons.sports_volleyball_outlined,),
          label: 'Partidos', 
          ),

           BottomNavigationBarItem(
          icon: Icon(Icons.calendar_month_outlined,),
           label: 'Campeonatos', //todo <== debe campeonatos
          ),

           BottomNavigationBarItem(
          icon: Icon(Icons.person_outline_outlined,),
           label: 'Perfil', //todo <== debe mostrar el perfil
          ),

      ],
     
     ),
      body:pages[pageIndex]
      
    );
     }
    );
  }
}




//!estos contenedor solo se tendrán mientras se construyen las pantallas respectivas */
class Page1 extends StatelessWidget {
  const Page1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(child: Text('En camino...',style: TextStyle(fontSize: 50,color: Colors.black),));
  }
}

