// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Profile_Manager_Screen extends StatelessWidget {
   
  const Profile_Manager_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      body: SizedBox(
        child: Column(
          children: 
          const [
           _ajustesPerfil() /*llamado a ajustes del perfil */
          ],
        ),
      )
    );
    }
  );
  }
}


//* PERFIL DEL MANAGER 
// ignore: unused_element
class _ajustesPerfil extends StatelessWidget {
  const _ajustesPerfil({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
       return SizedBox(
        height: 150.w,
        child: Column(
          children: 
          [

    //*FOTO DE PERFIL DEL MANAGER        
          CircleAvatar(
            backgroundColor: Maintheme.tertiaryColor,
            radius: 15.w,
            child: const FadeInImage(
                fadeInCurve: Curves.decelerate,
                placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
              image:AssetImage('assets/imgs/backgrounds/perfil.png') //todo <== aqui debe mostrar la foto de perfil del Manager
               ),
             ),
              
    //*NOMBRE DEL MANAGER
           Divider(height: 3.w,),
           Column(
               children: [
                 Text('manager.name',style: Maintheme.fonstApp.headline2,),//todo <== se debe hacer un llamado para el nombre del manager
  //*CORREO DEL MANAGER
            Text('correo.manager@gmail.com',style: Maintheme.fonstApp.headline5,), //todo <== se debe hacer un llamado para el nombre del manager


  //* BOTON DE EDITAR PERFIL
             Padding(
               padding:  EdgeInsets.only(top: 4.w,left: 15.w,right: 15.w),
               child: SizedBox(
                height: 15.w,
                width: 15.h,
                 child: ElevatedButton(
                  onPressed: (){Navigator.pushNamed(context, 'editprofilemanager');},// todo <== debe llevar a edicion de perfil
                   child: const Text('Editar perfil')),
               ),
             )
               ],
             
           ), 

  //*AJUSTES DEL PERFIL TIPO TEXTO 
          Padding(
            padding:  EdgeInsets.only(left: 5.w,right: 5.w,top: 15.w),
            child: Column(
              children: [
 //* AJUSTES DE TU EQUIPO  
                 Row(
                  children: [
                 const Icon(Icons.settings,color: Maintheme.quaternaryColor,),   
                 Padding(
                   padding:  EdgeInsets.only(left: 2.w),
                   child: GestureDetector(
                    onTap: (){Navigator.pushNamed(context, 'editarequipo');}, // todo <== debe llevar a a los ajustes de tu equipo
                    child: Text('Ajustes de tu equipo',style: Maintheme.fonstApp.headline2,)),
                 )
                ],
              ),
 //* INFORMACION DEL EQUIPO 
                  Divider(height: 7.w,color: Maintheme.transparents,), 
                 Row(
                  children: [
                 const Icon(Icons.sports_handball_sharp,color: Maintheme.quaternaryColor,),   
                 Padding(
                   padding:  EdgeInsets.only(left: 2.w),
                   child: GestureDetector(
                    onTap: (){Navigator.pushNamed(context, 'infoequipo');}, // todo <== debe llevar a a los ajustes de tu equipo
                    child: Text('Información tu equipo',style: Maintheme.fonstApp.headline2,)),
                 )
                ],
              ),


  //* CENTRO DE AYUDA 
            Divider(height: 7.w,color: Maintheme.transparents,),  
                Row(
                  children: [
                 const Icon(Icons.support_agent_outlined,color: Maintheme.quaternaryColor,),   
                 Padding(
                   padding: EdgeInsets.only(left: 2.w),
                   child: GestureDetector(
                    onTap: (){Navigator.pushNamed(context, 'centroayuda');}, // todo <== debe llevar a Centro de ayuda
                    child: Text('Centro de ayuda',style: Maintheme.fonstApp.headline2,)),
                 )
                ],
              ),  

  //* TERMINOS Y CONDICIONES
            Divider(height: 7.w,color: Maintheme.transparents,),  
                Row(
                  children: [
                 const Icon(Icons.article_outlined,color: Maintheme.quaternaryColor,),   
                 Padding(
                   padding: EdgeInsets.only(left: 2.w),
                   child: GestureDetector(
                    onTap: (){Navigator.pushNamed(context, 'terminosycondiones');}, // todo <== debe llevar a Términos y condiciones
                    child: Text('Términos y condiciones',style: Maintheme.fonstApp.headline2,)),
                 )
                ],
              ), 

    //* CERRAR SESION
            Divider(height: 7.w,color: Maintheme.transparents,),  
                Row(
                  children: [
                 const Icon(Icons.exit_to_app_outlined,color: Maintheme.quaternaryColor,),   
                 Padding(
                   padding: EdgeInsets.only(left: 2.w),
                   child: GestureDetector(
                    onTap: (){cerrarSesion(context);}, // todo <== debe llevar Cerrar sesión
                    child: Text('Cerrar sesión',style: Maintheme.fonstApp.headline2,)),
                 )
                ],
              ),          

              ],
            ),
          )

          ],
        ),
       );

      }
    );
  }



//*CUADRO DE DIALOGO PARA CERRAR SESION
    Future<String?> cerrarSesion(BuildContext context) {
    /**
     * Si el sistema operativo no es un iOS, entonces mostrará está alerta.
     * (Alertas para Android)
     */
    if (!Platform.isIOS) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: Maintheme.tertiaryColor,
          elevation: 0,
          title: Text(
            'Deseas cerrar la sesión?',
            style:Maintheme.fonstApp.headline2,
          ),
          content: Text('Cerrarás la sesión pero podrás ingresar nuevamente',
              style: Maintheme.fonstApp.subtitle1,),
          actions: <Widget>[
            SizedBox(
              child: Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  Text('Cancelar',style: Maintheme.fonstApp.subtitle1,)),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'login', 
                          (route) => false
                        );
                      },
                       child:  Text('Aceptar',style: Maintheme.fonstApp.subtitle1,))        
                ]
              ),
            ),
          ],
        ),
      );
    }

    /**
     * Si el sistema operativo es un iOS, entonces mostrará está otra alerta.
     */
    return showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text(
          'Deseas cerrar la sesión?',
          style: Maintheme.fonstApp.subtitle1,
        ),
        content: Text('Cerrarás la sesión pero podrás ingresar nuevamente',
            style:Maintheme.fonstApp.subtitle1,),
        actions: <Widget>[
          CupertinoDialogAction(
            child:  Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  const Text('Cancelar',style: TextStyle(color: Colors.blue,fontSize: 20))),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'login', 
                          (route) => false
                        );
                      },
                       child:  const Text('Aceptar',style: TextStyle(color: Colors.blue,fontSize: 20)))        
                ]
              ),
            
          ),
        ],
      ),
    );
  }
}