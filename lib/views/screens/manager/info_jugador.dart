// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Info_Jugador_Screen extends StatelessWidget {
   
  const Info_Jugador_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return  const Scaffold(
      body: CustomScrollView(
        slivers: [
          _CustomAppBar(), /*llamado a los ajustes del appBar */
        ],
      ),
    );
  }
}


//*AJUSTES DEL APPBAR
class  _CustomAppBar extends StatelessWidget {
  const  _CustomAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SliverAppBar(
     expandedHeight: size.height*.3,
     pinned: true,
     flexibleSpace: FlexibleSpaceBar(
      centerTitle: true,
      titlePadding: const EdgeInsets.all(0),
      title: Container(
  //* NOMBRE DEL JUGADOR
           width: double.infinity,  
           alignment: Alignment.bottomCenter,
           color: Colors.black12, /*sombra negra que se le agrega a las fotos */
        child: Text('nombre.jugador',style: Maintheme.fonstApp.subtitle1,) //todo <== llamado a nombre del jugador
      ),
  //* FOTO DEL JUGADOR
      background: const FadeInImage(
        placeholder:  AssetImage('assets/imgs/backgrounds/loading.gif',), // todo <== imagen de carga,posible cambio a NetworkImage
        image: AssetImage('assets/imgs/backgrounds/blanprofile.png'), // todo <== aqui se mostrará la foto de cada jugador y si no tiene foto se mostrará una imagen avatar, cambio a NetworkImage
        fit: BoxFit.cover,
        ),
     ),
    );
  }
}
