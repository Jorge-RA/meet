// ignore_for_file: camel_case_types, unused_element

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Edit_Equipo_ManagerScreen extends StatelessWidget {
   
  const Edit_Equipo_ManagerScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
      return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(),
      body: SizedBox(
        height: 190.w,
        child: SingleChildScrollView(
          child: Column(
            children: 
            const [
                _editarEquipo(), /*llamado a editar equipo */
            ],
          ),
        ),
      )
    );
      }
    );
  }
}

//* EDITAR EQUIPO
class _editarEquipo extends StatelessWidget {
  const _editarEquipo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
         return Sizer(
      builder: (context, orientation, deviceType) {
    return  Form(
        child: Padding(
          padding: EdgeInsets.only(left: 5.w,right:5.w),
            child: Column(
              children: [
                  Padding(
               padding: EdgeInsets.only(top: 15.w),
               child: Text('Editar equipo',style: Maintheme.fonstApp.headline2,),
             ),
  //*EDITAR FOTO DE PERFIL
            Stack(
              children: [
                Padding(
                padding:  EdgeInsets.only(top: 5.w),
                child: CircleAvatar(
                  backgroundColor: Maintheme.tertiaryColor,
                radius: 15.w,
                    child: const FadeInImage(
                      fadeInCurve: Curves.decelerate,
                      placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                    image:AssetImage('assets/imgs/backgrounds/perfil.png') //todo <== aqui debe mostrar la foto de perfil del equipo
                    ),
                  ),
              ),
   //*EDITAR Y AGREGAR FOTO DE PERFIL
              Padding(
                padding: EdgeInsets.only(top: 25.w,left: 20.w),
                child: IconButton(
                  onPressed: (){}, // todo <== debe agregar y seleccionar una foto para el perfil
                   icon: Icon(Icons.add_circle,
                   size: 10.w,),
                   )
              )
              ]
            ),
            Padding(
               padding: EdgeInsets.only(top: 2.w),
               child: Text('Edita el escudo de tu equipo',style: Maintheme.fonstApp.subtitle1,),
             ),       
               
    //*NOMBRE DE EQUIPO
                   Divider(height: 4.w,color: Maintheme.transparents,),
                   TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                        hintText: 'Editar nombre del equipo',
                        
                      ),
                   ),
  //*EDITAR SEDE DEL EQUIPO
            Divider(height: 6.w,color: Maintheme.transparents,),
                   TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                        hintText: 'Editar sede',
                       
                      ),
                   ),
                   
   //* NUMERO DE CONTACTO 
              Divider(height: 5.w,), 
              Row(
                children: <Widget>[
                     Flexible(
                      flex: 1,
                       child: TextFormField(
                        autocorrect: false,
                        autofocus: false,
                        style: Maintheme.fonstApp.subtitle1,
                        keyboardType: TextInputType.name,
                          decoration: const InputDecoration(
                            hintText: 'Editar número de contacto',
                          
                          ),
                       ),
                     ),

    //*BOTON DE AGREGAR JUGADOR                
            VerticalDivider(width: 2.w,),
           Container(
            height: 15.w,
            width: 40.w,
            decoration: BoxDecoration(
           gradient: Maintheme.gradiantStyle,
            borderRadius: BorderRadius.circular(20.w),
            boxShadow: [Maintheme.shadowBottons]
            ),
            child: Center(
              child: GestureDetector(
              onTap: (){// todo <== debe ir a la pantalla de agregar jugador
                    Navigator.pushNamed(context, 'agregarjugador');
              },
                child: Icon(Icons.add,size: 10.w,)),),
           ),   
             ],
              ),
           


  //*BOTON DE SIGUIENTE
           Divider(height: 10.w,color: Maintheme.transparents,),
          Container(
            height: 15.w,
            width: 90.w,
            decoration: BoxDecoration(
           gradient: Maintheme.gradiantStyle,
            borderRadius: BorderRadius.circular(20.w),
            boxShadow: [Maintheme.shadowBottons]
            ),
            child: Center(child: GestureDetector(
              onTap: (){//todo <== este boton debe guardar los cambios de edicion del equipo
               
              },
                child: Text('Guardar',style: Maintheme.fonstApp.headline1,),),),
           ), 
            Divider(height: 15.w,),                                                   
            ]),
        ),  
    );
   }
    );
      }
     );
    
  }
}