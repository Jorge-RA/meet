// ignore_for_file: camel_case_types, depend_on_referenced_packages
import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';
import 'package:card_swiper/card_swiper.dart';


class Info_Equipo_Screen extends StatelessWidget {
   
  const Info_Equipo_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    // final size =MediaQuery.of(context).size; 
    return  Scaffold(
      appBar: AppBar(),
      body: SizedBox( 
        height: double.infinity,
        width: double.infinity,
        child: Column(
          children:  const [
            _informacionDelEquipo(), /*llamado a foto del manager */
          
        
          ],
        ),
      )
    );
      }
    );
  }
}


//* INFORMACION DEL EQUIPO
class _informacionDelEquipo extends StatelessWidget {
  const _informacionDelEquipo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
        final size =MediaQuery.of(context).size; 
        return SizedBox(
          //height: 195.w,
          //  height: 90.w,
           width: double.infinity,
           child: Column(
            children: [
  //*FOTO DE PERFIL DEL MANAGER        
          Padding(
            padding:  EdgeInsets.only(top: 5.w),
            child: CircleAvatar(
              backgroundColor: Maintheme.tertiaryColor,
              radius: 15.w,
              child: const FadeInImage(
                  fadeInCurve: Curves.decelerate,
                   placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                image:AssetImage('assets/imgs/backgrounds/perfil.png') //todo <== aqui debe mostrar la foto de perfil del Manager
                 ),
               ),
          ),
  //*NOMBRE DEL MANAGER Y CORREO
       Text('Nombre.manager',style: Maintheme.fonstApp.headline2,), // todo <== aqui se va hacer el llamado a nombre del manager
        Text('Nombre.manager@gmail.com',style: Maintheme.fonstApp.headline5,), // todo <== aqui se va hacer el llamado al correo


  //* TEXTO DE JUGADORES Y VER TODOS LOS JUGADORES
         Row(
          children: 
          [
            Padding(
              padding:  EdgeInsets.only(top:10.w,left: 5.w),
              child: Text('Jugadores',style: Maintheme.fonstApp.headline2,),
            ),
                Padding(
                  padding: EdgeInsets.only(left: 40.w,top:15.w),
                  child: GestureDetector(
                    onTap: (){}, //todo <== debe llevar a ver todos los jugadores del equipo
                    child: Text('Ver todos +',style: Maintheme.fonstApp.subtitle1,)),
                ),
             ],
            ),
  //*CARD SWIPE DE JUGADORES
              Divider(height: 2.w,color: Maintheme.transparents,),
           SizedBox( 
              // height: 0.w,
             width:double.infinity,
              child: Column(
                children: 
                [
                 Swiper(
                  itemCount: 7, // todo <== por ahora solo se verán 7 jugadores y si preciona en el boton ver todos los llevará a ver la lista de jugadores 
                  layout:  SwiperLayout.STACK,
                  scrollDirection: Axis.horizontal,    
                  viewportFraction: 9,
                  curve: Curves.decelerate,                
                 itemWidth: size.width*.8,
                 itemHeight: size.height*.5,
                 itemBuilder: (BuildContext context, int index){
                  return GestureDetector(
                    onTap: (){Navigator.pushNamed(context, 'infojugador');}, // todo <== debe llevar a la informacion de cada jugador
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: const FadeInImage(
                        placeholder:  AssetImage('assets/imgs/backgrounds/loading.gif',), // todo <== imagen de carga,posible cambio a NetworkImage
                        image: AssetImage('assets/imgs/backgrounds/blanprofile.png'), // todo <== aqui se mostrará la foto de cada jugador y si no tiene foto se mostrará una imagen avatar, cambio a NetworkImage
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                 },
                ),
                ],
              ),
            ),          
            ],
           ),
        );
      }
     );
  }
}