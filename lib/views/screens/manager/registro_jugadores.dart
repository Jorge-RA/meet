// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';


//TOdo en esta pantalla se verán los jugadores registrados 

class Registro_Jugadores_Screen extends StatelessWidget {
   
  const Registro_Jugadores_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      body:SizedBox(
        height:190.w,
        child: Column(
          children: [
               Padding(
                 padding:  EdgeInsets.only(top: 15.w,left: 40.w),
                 child: Row(
                   children: [
                     Text('Jugadores',style: Maintheme.fonstApp.subtitle1,),
                  
                   ],
                 ),
               ),
               const _registroJugadores() /*llamado registro de jugadores */
          ],
        ),
      )
    );
      }
     );
  }
}

//*REGISTRO DE USUARIOS
class _registroJugadores extends StatefulWidget {
  const _registroJugadores({Key? key}) : super(key: key);

  @override
  State<_registroJugadores> createState() => _registroJugadoresState();
}

class _registroJugadoresState extends State<_registroJugadores> {
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
      return SizedBox(
      height: 170.w,
      child: ListView(
        children: 
        [
       //*REGISTRO DE JUGADORES 
          SizedBox(
            height: 15.w,
            width: 60.h,
            child: Padding(
              padding: EdgeInsets.only(left: 5.w,right: 5.w,),
              child: GestureDetector(
                onTap: (){ //todo <== debe llevar a la informacion del jugador

                },
                child: Card(
                  color: Maintheme.tertiaryColor,
                   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                   elevation: 0,
                   child: Row(
                    children: [
    //* FOTO DEL JUGADOR
                const Padding(
                 padding: EdgeInsets.all(8.0),
                 child: CircleAvatar(
                  backgroundColor: Maintheme.unselect2,
                  child: FadeInImage(
                    fit: BoxFit.cover,
                    placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                  image:AssetImage('assets/imgs/backgrounds/perfilJugador.png') //todo <== aqui debe mostrar la foto de perfil del jugador
                  ),
                          ),
                           ),
                          
    //* NOMBRE DEL JUGADOR 1
                           Padding(
                 padding: const EdgeInsets.all(8.0),
                 //! falta poner que el texto si es muy largo, se divida en 2
                 child: Text('jugador.name',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada al nombre del team
                    ),                
                    ],
                   ),
                ),
              ),
            ),
          ),

  //* JUGADOR 2
  //! BORRAR DESPUES
         SizedBox(
            height: 15.w,
            width: 60.h,
            child: Padding(
              padding: EdgeInsets.only(left: 5.w,right: 5.w,),
              child: GestureDetector(
                onTap: (){},// todo <== debe llevar a la información del jugador
                child: Card(
                  color: Maintheme.tertiaryColor,
                   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                   elevation: 0,
                   child: Row(
                    children: [
  //* FOTO DEL JUGADOR
                const Padding(
                 padding: EdgeInsets.all(8.0),
                 child: CircleAvatar(
                  backgroundColor: Maintheme.unselect2,
                  child: FadeInImage(
                    fit: BoxFit.cover,
                    placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                  image:AssetImage('assets/imgs/backgrounds/PerfilJugadores.png') //todo <== aqui debe mostrar la foto de perfil del jugador
                  ),
                          ),
                           ),
                          
  //* NOMBRE DEL JUGADOR 1
                           Padding(
                 padding: const EdgeInsets.all(8.0),
                 //! falta poner que el texto si es muy largo, se divida en 2
                 child: Text('jugador.name',style: Maintheme.fonstApp.subtitle1,maxLines: 3,), // todo <== aqui se debe hacer la llamada al nombre del team
                    ),                
                    ],
                   ),
                ),
              ),
            ),
          ),
  //*BOTON DE REGISTRAR JUGADORES
           Padding(
             padding:  EdgeInsets.only(top:80.w,left: 70.w),
             child: IconButton(
              onPressed: (){ //todo <== debe llevar al textformfield de registro 

                Navigator.pushNamed(context, 'registrojugadorform');              
              }, 
              icon:const Icon(Icons.add_circle,color: Maintheme.secondaryColor,),
              iconSize: 20.w,
          ),
           ), 

   //*BOTON DE SIGUIENTE 
   SizedBox(
     height: 20.w,
     width: 50.w,
     child: Padding(
       padding:  EdgeInsets.only(left: 15.w,right: 15.w,top: 4.w),
       child: ElevatedButton(
        onPressed: (){Navigator.pushNamed(context, 'appbarManager');}, 
        child: const Text('Siguiente'),
       ),
     ),
   ),          
        ],
      ),
      );
  }
  );
  }
}