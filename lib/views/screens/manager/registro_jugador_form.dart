// todo: está pantalla se registrará al jugador de cada equipo

// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';
class Registro_Jugador_Form_Screen extends StatelessWidget {
   
  const Registro_Jugador_Form_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(),
      body:SingleChildScrollView(
        child: Column(
          children: 
          [
             Text('Registro de jugadores',style: Maintheme.fonstApp.headline4,),
             const _registroJugador()
          ],
        ),
      )
    );
      }
     );
  }
}

//* REGISTRO DE JUGADOR
class _registroJugador extends StatelessWidget {
  const _registroJugador({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
     return Form(
        child: Padding(
          padding: EdgeInsets.only(left: 5.w,right: 5.w,top: 10.w),
          child: Column(
       children:  [
   //*FOTO DE PERFIL
              Stack(
                children: [
                  Padding(
                  padding:  EdgeInsets.only(top: 5.w),
                  child: CircleAvatar(
                    backgroundColor: Maintheme.tertiaryColor,
                  radius: 15.w,
                      child: const FadeInImage(
                        fadeInCurve: Curves.decelerate,
                        placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                      image:AssetImage('assets/imgs/backgrounds/perfil.png') //todo <== aqui debe mostrar la foto de perfil del jugador
                      ),
                    ),
                ),
   //* AGREGAR FOTO DE PERFIL
                Padding(
                  padding: EdgeInsets.only(top: 25.w,left: 20.w),
                  child: IconButton(
                    onPressed: (){}, // todo <== debe agregar y seleccionar una foto para el perfil
                     icon: Icon(Icons.add_circle,color: Maintheme.secondaryColor,size: 10.w,),
                     )
                )
                ]
              ),
               Padding(
                 padding: EdgeInsets.only(top: 3.w),
                 child: Text('Foto de perfil',style: Maintheme.fonstApp.subtitle1,),
               ),

      //*NOMBRE DEL JUGADOR   
                     Divider(height: 3.w,color: Maintheme.transparents,),       
                    TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      textCapitalization: TextCapitalization.sentences,
                      decoration: const InputDecoration(
                        hintText: 'Nombre completo',
                        
                      ),
                   ),
      //*NUMERO EDAD
               Divider(height: 5.w,color: Maintheme.transparents,),
               TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      textCapitalization: TextCapitalization.sentences,
                      decoration: const InputDecoration(
                        hintText: 'Edad',
                       
                      ),
                   ),

        //* NUMERO DE DOCUMENTO
               Divider(height: 5.w,color: Maintheme.transparents,),
               TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      textCapitalization: TextCapitalization.sentences,
                      decoration: const InputDecoration(
                        hintText: 'Numero de documento',
                        
                      ),
                   ),   

       //* GENERO
               Divider(height: 5.w,color: Maintheme.transparents,),
               TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      textCapitalization: TextCapitalization.sentences,
                      decoration: const InputDecoration(
                        hintText: 'Género',
                      
                      ),
                   ),

        //* TELEFONO
               Divider(height: 5.w,color: Maintheme.transparents,),
               TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      textCapitalization: TextCapitalization.sentences,
                      decoration: const InputDecoration(
                        hintText: 'Télefono',
                       
                      ),
                   ),


     //* BOTON DE INGRESAR 
            Divider(height: 5.w, color: Maintheme.transparents,),       
               SizedBox(
                height: 13.w,
                width: 60.w,
                child: ElevatedButton(
                  onPressed: (){},// todo <== debe registrar a los jugadores
                   child: const Text('Registrar')
                  ),
               ), 
             Divider(height: 8.w, color: Maintheme.transparents,),              
             ],
          ),
        )  
       );
      }
    );   
  }
}

