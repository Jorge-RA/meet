// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class AppBar_Manager_Screen extends StatefulWidget {
   
  const AppBar_Manager_Screen({Key? key}) : super(key: key);

  @override
  State<AppBar_Manager_Screen> createState() => _AppBar_Manager_ScreenState();
}

class _AppBar_Manager_ScreenState extends State<AppBar_Manager_Screen> {

     int pageIndeManager = 0; /*valor de las paginas */

   final pagesManager = [ /*contenedores de las pagina que se mostrarán en el bottom navegator */

   
       const Page0(), //! falta hacer la Page 1
       const Lista_Torneos_Screen(),
       const Profile_Manager_Screen() 
  ];
     @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('MEET'),
        
        ),

  //*BOTON DE NAVEGACION
     bottomNavigationBar: BottomNavigationBar(
     selectedItemColor: Maintheme.quaternaryColor, /*cambiar color al seleccionar */
    unselectedItemColor: Maintheme.unselect,/*color del que no está seleccionado */
   showUnselectedLabels: false, /*solo muestra las letras del icon seleccionado */
      currentIndex: pageIndeManager,
      onTap: (index)=>setState(() =>pageIndeManager = index, ),
      items: 
      const [
        BottomNavigationBarItem( /*boton de ver partidos */
          icon: Icon(Icons.sports_volleyball_outlined,),
          label: 'Partidos', 
          ),

           BottomNavigationBarItem(
          icon: Icon(Icons.calendar_month_outlined,),
           label: 'Campeonatos', //todo <== debe campeonatos
          ),

           BottomNavigationBarItem(
          icon: Icon(Icons.person_outline_outlined,),
           label: 'Perfil', //todo <== debe mostrar el perfil
          ),

      ],
     
     ),
      body:pagesManager[pageIndeManager]
      
    );
     }
    );
  }

}

//!estos contenedor solo se tendrán mientras se construyen las pantallas respectivas */
class Page0 extends StatelessWidget {
  const Page0({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(child: Text('En camino...',style: TextStyle(fontSize: 50,color: Colors.black),));
  }
}


