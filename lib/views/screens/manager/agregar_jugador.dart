// ignore_for_file: camel_case_types, unused_element

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Editar_Jugador_Manager_Screen extends StatelessWidget {
   
  const Editar_Jugador_Manager_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(),
      body: SizedBox(
        height: 190.w,
        child: SingleChildScrollView(
          child: Column(
            children: 
            const [
               _agregarJugador() /*llamado a agregar jugador */
            ],
          ),
        ),
      )
    );
      }
     );
  }
}

//*TEXFORMFIELD PARA AGREGAR JUGADOR
class _agregarJugador extends StatelessWidget {
  const _agregarJugador({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
        return SizedBox(
          child: Form(
            child: Padding(
              padding: EdgeInsets.only(left: 5.w,right:5.w),
              child: Column(
                children: [
                 //*! arreglando paddin    
        Text('Agregar jugador',style: Maintheme.fonstApp.headline2,),
               

        //*EDITAR FOTO DE PERFIL
            Stack(
              children: [
                Padding(
                padding:  EdgeInsets.only(top: 5.w,),
                child: CircleAvatar(
                  backgroundColor: Maintheme.tertiaryColor,
                radius: 15.w,
                    child: const FadeInImage(
                      fadeInCurve: Curves.decelerate,
                      placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                    image:AssetImage('assets/imgs/backgrounds/perfil.png') //todo <== aqui debe mostrar la foto de perfil deljugador
                    ),
                  ),
              ),
   //*EDITAR Y AGREGAR FOTO DE PERFIL
              Padding(
                padding: EdgeInsets.only(top: 25.w,),
                child: IconButton(
                  onPressed: (){}, // todo <== debe agregar y seleccionar una foto para el perfil
                   icon: Icon(Icons.add_circle,color: Maintheme.secondaryColor,size: 10.w,),
                   )
              ),
              ],
              
            ),
             
             
        Text('Agregar foto',style: Maintheme.fonstApp.subtitle1,),
             
    //*NOMBRE DEl JUGADOR
                   Divider(height: 4.w,color: Maintheme.transparents,),
                   TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                        hintText: 'Nombre del jugador'),
                   ),
                   
       //*CORREO ELECTRONICO
          Divider(height: 4.w,color: Maintheme.transparents,),
                   TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.emailAddress,
                      decoration: const InputDecoration(
                        hintText: 'Correo'),
                   ),

      //* FECHA DE NACIMIENTO
          Divider(height: 4.w,color: Maintheme.transparents,),
                   TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.datetime,
                      decoration: const InputDecoration(
                        hintText: 'Fecha de nacimiento', ),
                   ), 
    //* CIUDAD
               Divider(height: 4.w,color: Maintheme.transparents,),
                   TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                        hintText: 'Ciudad',
                      
                      ),
                   ), 

    //* GENERO
               Divider(height: 4.w,color: Maintheme.transparents,),
                   TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                        hintText: 'Genero'),
                   ),
      //* BOTON DE REGISTRAR JUGADOR             
                 Divider(height:8.w,color: Maintheme.transparents,),           

            Container(
            height: 15.w,
            width: 90.w,
            decoration: BoxDecoration(
           gradient: Maintheme.gradiantStyle,
            borderRadius: BorderRadius.circular(20.w),
            boxShadow: [Maintheme.shadowBottons]
            ),
            child: Center(child: GestureDetector(//todo<== al precionar debe validar la seleccion de la persona y llevarlo a la pantalla que le corresponde
              onTap: (){
               
              },
              child: Text('Registrar',style: Maintheme.fonstApp.headline1,),),),
           ),
                    Divider(height:8.w,color: Maintheme.transparents,),                                  
               ],
              ),
            )
            ),
        );
      }
     );
  }
}