// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Registro_Equipo_Screen extends StatelessWidget {
   
  const Registro_Equipo_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      body: SizedBox(  
        height: 190.w,
        child: SingleChildScrollView(
          child: Column(
            children: 
            const [
             
              _registroEquipo() /*llamado a registro del grupo */
            ],
          ),
        ),
      )
    );
      }
    );
  }
}

//* TEXTFORMFIELD PARA REGISTRO 
class _registroEquipo extends StatelessWidget {
  const _registroEquipo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Form(
        child: Padding(
          padding: EdgeInsets.only(top: 10.w, left: 5.w,right:5.w),
            child: Column(
              children: [
                  Padding(
               padding: EdgeInsets.only(top: 15.w),
               child: Text('Tu equipo',style: Maintheme.fonstApp.headline2,),
             ),
  //*FOTO DE PERFIL
            Stack(
              children: [
                Padding(
                padding:  EdgeInsets.only(top: 5.w),
                child: CircleAvatar(
                  backgroundColor: Maintheme.tertiaryColor,
                radius: 15.w,
                    child: const FadeInImage(
                      fadeInCurve: Curves.decelerate,
                      placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                    image:AssetImage('assets/imgs/backgrounds/perfil.png') //todo <== aqui debe mostrar la foto de perfil del equipo
                    ),
                  ),
              ),
   //* AGREGAR FOTO DE PERFIL
              Padding(
                padding: EdgeInsets.only(top: 25.w,left: 20.w),
                child: IconButton(
                  onPressed: (){}, // todo <== debe agregar y seleccionar una foto para el perfil
                   icon: Icon(Icons.add_circle,color: Maintheme.secondaryColor,size: 10.w,),
                   )
              )
              ]
            ),
            Padding(
               padding: EdgeInsets.only(top: 2.w),
               child: Text('Añade el escudo de tu equipo',style: Maintheme.fonstApp.subtitle1,),
             ),       
               
    //*NOMBRE DE EQUIPO
                   Divider(height: 4.w,color: Maintheme.transparents,),
                   TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                        hintText: 'Nombre del equipo',
                        
                      ),
                   ),
  //* SEDE DEL EQUIPO
            Divider(height: 6.w,color: Maintheme.transparents,),
                   TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                        hintText: 'Sede',
                       
                      ),
                   ),

   //* NUMERO DE CONTACTO 
              Divider(height: 6.w,color: Maintheme.transparents,),
                   TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.name,
                      decoration: const InputDecoration(
                        hintText: 'Número de contacto',
                       
                      ),
                   ), 
  //*BOTON DE SIGUIENTE
                  Divider(height: 10.w,color: Maintheme.transparents,),
             SizedBox(
              height: 15.w,
              width: 60.h,
              child: ElevatedButton(
                onPressed:(){//todo <== este boton debe hacer el registro del equipo 
                 Navigator.pushNamed(context, 'registrojugador');
                },
                child: const Text('Siguiente')
                ),
             )                                                 
              ],
            ),
         
        ),
       
    );
      }
    );
  }
}