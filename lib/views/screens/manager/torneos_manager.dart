// ignore_for_file: camel_case_types, unused_element

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Lista_Torneos_Screen extends StatelessWidget {
   
  const Lista_Torneos_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      body:SizedBox(
        child: Column(
          children: [
              Text('Torneos',style: Maintheme.fonstApp.headline4,),
            Padding(
              //* texto #1
              padding:  EdgeInsets.only(right: 62.w,top: 5.w),
              child: Text('Tus torneos',style: Maintheme.fonstApp.subtitle1,),
            ),
            const _tusTorneosManager(), /*llamado a los torneos, torneos con inscripciones abierta y torneos iniciados */
             
          ],
        ),
      )
    );
      }
     );
  }
}

//*TUS TORNEOS 
class _tusTorneosManager extends StatelessWidget {
  const _tusTorneosManager({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
        return SizedBox(
          height: 100.w, //!posible error
        child: ListView(
          scrollDirection: Axis.vertical,
          children: 
          [

      //* TUS TORNEOS
             Padding(
               padding:  EdgeInsets.only(top: 2.w,left: 5.w,right: 5.w),
               child: GestureDetector(
                onTap: (){}, // todo <== debe llevar a la informaciion del torneo o partidos del torneo
                 child: Card(
                     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                     elevation:0,
                     color: Maintheme.tertiaryColor,
                     child: ListTile(
                       title: Text('Torneo 1',style: Maintheme.fonstApp.subtitle1,), //todo <== aqui debe ir el nombre delo de los torneos, creados por el organizador 
                       leading: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
                      backgroundColor: Maintheme.unselect2,
                      child: Text('Ftsas'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
                  ),
                     ),
                 ),
               ),
             ),
    //* TORNEOS CON INSCRIPCIONES ABIERTAS
                Padding(
              //* texto #2
              padding:  EdgeInsets.only(left: 10.w,top: 5.w),
              child: Text('Torneos con inscripción abierta',style: Maintheme.fonstApp.subtitle1,),
            ),
                 Padding(
               padding:  EdgeInsets.only(top: 2.w,left: 5.w,right: 5.w),
               child: GestureDetector(
                onTap: (){}, // todo <== debe llevar a la informaciion del torneo,como donde se juega, premio,mecanica de juego
                 child: Card(
                     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                     elevation:0,
                     color: Maintheme.tertiaryColor,
                     child: ListTile(
                       title: Text('Torneo 2',style: Maintheme.fonstApp.subtitle1,), //todo <== aqui debe ir el nombre delo de los torneos, creados por el organizador 
                       leading: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
                      backgroundColor: Maintheme.unselect2,
                      child: Text('Qw'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
                  ),
                     ),
                 ),
               ),
             ),

    //* TORNEOS INICIADOS
                     Padding(
              //* texto #2
              padding:  EdgeInsets.only(left: 10.w,top: 5.w),
              child: Text('Torneos iniciados',style: Maintheme.fonstApp.subtitle1,),
            ),
                Padding(
               padding:  EdgeInsets.only(top: 2.w,left: 5.w,right: 5.w),
               child: GestureDetector(
                onTap: (){}, // todo <== debe llevar a la informaciion del torneo o partidos del torneo
                 child: Card(
                     shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                     elevation:0,
                     color: Maintheme.tertiaryColor,
                     child: ListTile(
                       title: Text('Torneo 3',style: Maintheme.fonstApp.subtitle1,), //todo <== aqui debe ir el nombre delo de los torneos, creados por el organizador 
                       leading: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
                      backgroundColor: Maintheme.unselect2,
                      child: Text('TGTsas'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
                  ),
                     ),
                 ),
               ),
             ),      
          ],
        ),
        );
     }
  );
  }
}





