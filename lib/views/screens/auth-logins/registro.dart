// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';
import 'package:meet_new/models/register_model.dart';
import 'package:meet_new/models/validations_model.dart';
import 'package:meet_new/views/components/custom_button.dart';
import 'package:meet_new/views/components/custom_text_form_field.dart';

class RegitroUser_Screen extends StatelessWidget {
  const RegitroUser_Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
          appBar: AppBar(),
          body: SingleChildScrollView(
            child: Column(
              children: [
                Divider(
                  height: 10.w,
                ), //!quitar
                const _registroUser() /*llamado a formulario de registro */
              ],
            ),
          ));
    });
  }
}

//* REGISTRO DE USUARIOS
class _registroUser extends StatefulWidget {
  const _registroUser({Key? key}) : super(key: key);

  @override
  State<_registroUser> createState() => _registroUserState();
}

class _registroUserState extends State<_registroUser> {
  bool _oscureText = false; /*visibilidad al escribir la contraseña */
  final formKey = GlobalKey<FormState>();
  final userRegister =
      UserRegister(); //Modelo que mantiene los datos del usuario a registrar
  @override
  void initState() {
    /*el codigo de visibilidad se inicia al iniciar la pantalla */
    // TODo: implement initState
    _oscureText = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return Padding(
        padding: EdgeInsets.only(top: 10.w, left: 5.w, right: 5.w),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              //* INGRESAR NOMBRE COMPLETO
              CustomTextFomrField(
                hintText: 'Nombre Completo',
                validator: (value) => Validations.isEmpty(value),
                onChanged: (value) => userRegister.username = value,
              ),
              const Divider(),
              CustomTextFomrField(
                hintText: 'Email',
                validator: (value) => Validations.email(value),
                onChanged: (value) => userRegister.email = value,
              ),
              const Divider(),
              CustomTextFomrField(
                hintText: 'Contraseña',
                isPsw: true,
                validator: (value) => Validations.isEmpty(value),
                onChanged: (value) => userRegister.password = value,
              ),
              const Divider(),
              CustomTextFomrField(
                hintText: 'Repetir contraseña',
                isPsw: true,
                validator: (value) => Validations.equalPassword(value, userRegister.password),
                onChanged: (value) {},
              ),
              //*BOTON DE INGRESO
              const Divider(),
              CustomButton(
                text: 'Registrate',
                onTap: (){
                  if(formKey.currentState!.validate()){
                    Navigator.of(context).pushNamed('rol', arguments: userRegister);
                  }
                  
                },
              ),

              //* SI TIENES CUENTA?
              TextButton(
                  onPressed: () {
                    //todo <== debe llevar al screen de iniciar sesion
                    Navigator.pushNamed(context, 'login');
                  },
                  child: Text(
                    'Ya tienes cuenta?',
                    style: Maintheme.fonstApp.subtitle1,
                  ))
            ],
          ),
        ),
      );
    });
  }
}
