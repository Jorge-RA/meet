// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';
import 'package:meet_new/models/validations_model.dart';

class Login_Screen extends StatelessWidget {
  const Login_Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return Scaffold(
          body: SizedBox(
        height: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Lottie.asset('assets/imgs/gits/ballsport.json', height: 80.w),
              /*animacion del login tipo git */

              Divider(
                height: 3.w,
              ),
              Center(
                  child: Text(
                'Bienvenido a Meet',
                style: Maintheme.fonstApp.headline2,
              )),
              const _loginForm(), /*llamado a formulario */
            ],
          ),
        ),
      ));
    });
  }
}

//*login y formulario */
class _loginForm extends StatefulWidget {
  const _loginForm({Key? key}) : super(key: key);

  @override
  State<_loginForm> createState() => _loginFormState();
}

class _loginFormState extends State<_loginForm> {
  bool _oscureText = false; /*visibilidad al escribir la contraseña */
  final formKey = GlobalKey<FormState>();
  Map<String, String> formData = {
    'email': '',
    'password': ''
  };
  @override
  void initState() {
    /*el codigo de visibilidad se inicia al iniciar la pantalla */
    // TODo: implement initState
    _oscureText = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return SizedBox(
        height: 100.w,
        child: Form(
            key: formKey,
            child: Padding(
              padding: EdgeInsets.only(top: 10.w, left: 5.w, right: 5.w),
              child: Column(
                children: [
                  //* INGRESAR CORREO
                  TextFormField(
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      hintText: 'Email',
                    ),
                    validator: (value) => Validations.email(value),
                    onChanged: (value) => formData['email'] = value,
                  ),

                  //* INGRESAR CONTRASEÑA
                  Divider(
                    height: 5.w,
                    color: Maintheme.transparents,
                  ),
                  TextFormField(
                    obscureText: _oscureText,
                    /*contraseña visible o no */
                    autocorrect: false,
                    autofocus: false,
                    style: Maintheme.fonstApp.subtitle1,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(
                            () {
                              /*pasa a ser falso o verdadero */
                              _oscureText = !_oscureText;
                            },
                          );
                        },
                        icon: Icon(
                          /*al tocar el boton cambia de visible a no visible */
                          _oscureText
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                        ),
                      ),
                      hintText: 'Contraseña',
                    ),
                    validator: (value) => Validations.isEmpty(value),
                    onChanged: (value) => formData['password'] = value,
                  ),

                  //*BOTON DE INGRESO efecto gradiante
                  Divider(
                    height: 5.w,
                    color: Maintheme.transparents,
                  ),
                  Container(
                    height: 15.w,
                    width: 90.w,
                    decoration: BoxDecoration(
                        gradient: Maintheme.gradiantStyle,
                        borderRadius: BorderRadius.circular(20.w),
                        boxShadow: [Maintheme.shadowBottons]),
                    child: Center(
                      child: GestureDetector(
                        onTap: () {
                          if (formKey.currentState!.validate()) {
                            Navigator.pushNamedAndRemoveUntil(context, 'homeorganizador', (route) => false);
                            //? La data del los texformfield está en "formData"
                            print(formData);
                          }
                        },
                        child: Text(
                          'Iniciar sesión',
                          style: Maintheme.fonstApp.headline1,
                        ),
                      ),
                    ),
                  ),

                  //* SI TIENES CUENTA?
                  TextButton(
                    onPressed: () {
                      //todo <== debe llevar al screen de registro
                      Navigator.pushNamed(context, 'registro');
                    },
                    child: Text(
                      'Aun no tienes cuenta?',
                      style: Maintheme.fonstApp.subtitle1,
                    ),
                  ),
                ],
              ),
            )),
      );
    });
  }
}
