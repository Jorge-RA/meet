
// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Selec_Rol_Screen extends StatelessWidget {
   
  const Selec_Rol_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          Center(child: Text('¿Quien eres?',style: Maintheme.fonstApp.headline2,)),
          const _selectoRol()/*llamado a selector de roles */
        ],
      )
    );
  }
}

//* SELECCION DE ROL
class _selectoRol extends StatefulWidget {
  const _selectoRol({Key? key}) : super(key: key);

  @override
  State<_selectoRol> createState() => _selectoRolState();
}

class _selectoRolState extends State<_selectoRol> {

static bool _selectRoles1 = false; /*circulo de seleccion de rol Organizador */
static bool _selectRoles2 = false; /*circulo de seleccion de rol  Manager*/
static bool _selectRoles3 = false; /*circulo de seleccion de rol  General*/

   @override
  void initState() {  /*el circulo de seleccion se inicia o cambia de estado */
    // TODo: implement initState
   _selectRoles1 = true;
   _selectRoles2 = true;
   _selectRoles3 = true;
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return Column(
      children: [

        //* ROL ORGANIZADOR
        Divider(height: 15.w,color: Maintheme.transparents,),
       GestureDetector(
        onTap:(){ //todo <== al precionar debe seleccionar el rol y cambiar el icono de color verde
                 //todo <== debe llevar a la pantalla de organizador
           setState(() {
              if(_selectRoles1 = true){
                _selectRoles2 =! false;
                _selectRoles3 =!false;  
              }
             _selectRoles1 =!_selectRoles1; /*al tocar este boton se selecciona el rol */

              cuadroDialogOrganizador(context);  /*abre un cuadro de dialogo que muestra informacion del rol y re dirige hacia la pantalla que le corresponde */                            
           });
        }, 
         child: Container(
          height: 20.w,
          width: 90.w,
          // ignore: sort_child_properties_last
          child: Row(
          children: [ 
            VerticalDivider(width: 5.w,), 
            
            Icon(_selectRoles1?  Icons.circle_outlined : Icons.circle,color: Maintheme.secondaryColor,  shadows: [Maintheme.shadows],),

            Text('Organizador',style: Maintheme.fonstApp.subtitle1,)
          ],),
          decoration: BoxDecoration(
            color: Maintheme.tertiaryColor,
            borderRadius: BorderRadius.circular(20)
          ),
         ),
       ),
       
       //*ROL MANAGER
         Divider(height: 5.w,color: Maintheme.transparents,),
       GestureDetector(
        onTap:(){ //todo <== al precionar debe seleccionar el rol y cambiar el icono de color verde
                 //todo <=== debe llevar a la pantalla de manager
           setState(() {
                   if(_selectRoles2 = true){
                _selectRoles1 =! false;
                _selectRoles3 =!false;
                
              }
             _selectRoles2 =!_selectRoles2; /*al tocar este boton se selecciona el rol */
             cuadroDialogManager(context); /*cuadro de dialogo para manager */
           });
        }, 
         child: Container(
          height: 20.w,
          width: 90.w,
          // ignore: sort_child_properties_last
          child: Row(
          children: [ 
            VerticalDivider(width: 5.w,), 
            
            Icon(_selectRoles2?  Icons.circle_outlined : Icons.circle, color: Maintheme.secondaryColor,  shadows: [Maintheme.shadows], ),

            Text('Manager',style: Maintheme.fonstApp.subtitle1,)
          ],),
          decoration: BoxDecoration(
            color: Maintheme.tertiaryColor,
            borderRadius: BorderRadius.circular(20)
          ),
         ),
       ),
        
        //* ROL DE GENERAL
         Divider(height: 5.w,color: Maintheme.transparents,),
       GestureDetector(
        onTap:(){ //todo <== al precionar debe seleccionar el rol y cambiar el icono de color verde
                  //todo <=== debe llevar a la pantalla de usuario

           setState(() {
                 if(_selectRoles3 = true){
                _selectRoles2 =! false;
                _selectRoles1=!false;
                
              }
             _selectRoles3 =!_selectRoles3; /*al tocar este boton se selecciona el rol */
             cuadroDialogUsers(context); /*cuadro de dialogo para usuarios */
           });
        }, 
         child: Container(
          height: 20.w,
          width: 90.w,
          // ignore: sort_child_properties_last
          child: Row(
          children: [ 
            VerticalDivider(width: 5.w,), 
            
            Icon(_selectRoles3?  Icons.circle_outlined : Icons.circle,color: Maintheme.secondaryColor, shadows: [Maintheme.shadows],),

            Text('General',style: Maintheme.fonstApp.subtitle1,)
          ],),
          decoration: BoxDecoration(
            color: Maintheme.tertiaryColor,
            borderRadius: BorderRadius.circular(20)
          ),
         ),
       ),

       //* BOTON DE INGRESAR O SIGUIENTE
       Divider(height: 50.w,),

         Container(
            height: 15.w,
            width: 90.w,
            decoration: BoxDecoration(
           gradient: Maintheme.gradiantStyle,
            borderRadius: BorderRadius.circular(20.w),
            boxShadow: [Maintheme.shadowBottons]
            ),
            child: Center(child: GestureDetector(//todo<== al precionar debe validar la seleccion de la persona y llevarlo a la pantalla que le corresponde
              onTap: (){
               
              },
                child: Text('Siguiente',style: Maintheme.fonstApp.headline1,),),),
           ),
      ],
    );
      }
     );
  }
  
    //*CUADRO DE DIALOGO #1 ORGANIZADOR 
     //! Escribir la informacion que mostrará el cuadro de dialogo
    Future<String?> cuadroDialogOrganizador(BuildContext context) {
    /**
     * Si el sistema operativo no es un iOS, entonces mostrará está alerta.
     * (Alertas para Android)
     */
    if (!Platform.isIOS) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: Maintheme.tertiaryColor,
          elevation: 0,
          title: Text(
            'Seleccionar rol',
            style:Maintheme.fonstApp.headline2,
          ),
          content: Text('Podrás crear torneos, editarlos y ver la información, del torneo',
              style: Maintheme.fonstApp.subtitle1,),
          actions: <Widget>[
            SizedBox(
              child: Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  Text('Cancelar',style: Maintheme.fonstApp.subtitle1,)),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'homeorganizador', 
                          (route) => false
                        );
                      },
                       child:  Text('Seleccionar',style: Maintheme.fonstApp.subtitle1,))        
                ]
              ),
            ),
          ],
        ),
      );
    }

    /**
     * Si el sistema operativo es un iOS, entonces mostrará está otra alerta.
     */
    return showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text(
          'Seleccionar rol',
          style: Maintheme.fonstApp.subtitle1,
        ),
        content: Text('Podrás crear torneos, editarlos y ver la información, del torneo',
            style:Maintheme.fonstApp.subtitle1,),
        actions: <Widget>[
          CupertinoDialogAction(
            child:  Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  const Text('Cancelar',style: TextStyle(color: Colors.blue,fontSize: 20))),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'homeorganizador', 
                          (route) => false
                        );
                      },
                       child:  const Text('Seleccionar',style: TextStyle(color: Colors.blue,fontSize: 20),))        
                ]
              ),
            
          ),
        ],
      ),
    );
  }


   //*CUADRO DE DIALOGO #2 MANAGER
    //! Escribir la informacion que mostrará el cuadro de dialogo
     Future<String?> cuadroDialogManager(BuildContext context) {
    /**
     * Si el sistema operativo no es un iOS, entonces mostrará está alerta.
     * (Alertas para Android)
     */
    if (!Platform.isIOS) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: Maintheme.tertiaryColor,
          elevation: 0,
          title: Text(
            'Seleccionar rol',
            style:Maintheme.fonstApp.headline2,
          ),
          content: Text('Podrás crear quipos, editarlos y ver la información, del torneo,etc',
              style: Maintheme.fonstApp.subtitle1,),
          actions: <Widget>[
            SizedBox(
              child: Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  Text('Cancelar',style: Maintheme.fonstApp.subtitle1,)),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'registroequipo', 
                          (route) => false
                        );
                      },
                       child:  Text('Seleccionar',style: Maintheme.fonstApp.subtitle1,))        
                ]
              ),
            ),
          ],
        ),
      );
    }

    /**
     * Si el sistema operativo es un iOS, entonces mostrará está otra alerta.
     */
    return showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text(
          'Seleccionar rol',
          style: Maintheme.fonstApp.subtitle1,
        ),
        content: Text('Podrás crear quipos, editarlos y ver la información, del torneo,etc',
            style:Maintheme.fonstApp.subtitle1,),
        actions: <Widget>[
          CupertinoDialogAction(
            child:  Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  const Text('Cancelar',style: TextStyle(color: Colors.blue,fontSize: 20))),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'registroequipo', 
                          (route) => false
                        );
                      },
                       child:  const Text('Seleccionar',style: TextStyle(color: Colors.blue,fontSize: 20),))        
                ]
              ),
            
          ),
        ],
      ),
    );
  }


   //* CUADRO DE DIALOGO #3 USERS
   //! Escribir la informacion que mostrará el cuadro de dialogo
    Future<String?> cuadroDialogUsers(BuildContext context) {
    /**
     * Si el sistema operativo no es un iOS, entonces mostrará está alerta.
     * (Alertas para Android)
     */
    if (!Platform.isIOS) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: Maintheme.tertiaryColor,
          elevation: 0,
          title: Text(
            'Seleccionar rol',
            style:Maintheme.fonstApp.headline2,
          ),
          content: Text('Podrás crear quipos, editarlos y ver la información, del torneo,etc',
              style: Maintheme.fonstApp.subtitle1,),
          actions: <Widget>[
            SizedBox(
              child: Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  Text('Cancelar',style: Maintheme.fonstApp.subtitle1,)),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'vertorneos', 
                          (route) => false
                        );
                      },
                       child:  Text('Seleccionar',style: Maintheme.fonstApp.subtitle1,))        
                ]
              ),
            ),
          ],
        ),
      );
    }

    /**
     * Si el sistema operativo es un iOS, entonces mostrará está otra alerta.
     */
    return showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text(
          'Seleccionar rol',
          style: Maintheme.fonstApp.subtitle1,
        ),
        content: Text('Podrás crear quipos, editarlos y ver la información, del torneo,etc',
            style:Maintheme.fonstApp.subtitle1,),
        actions: <Widget>[
          CupertinoDialogAction(
            child:  Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  const Text('Cancelar',style: TextStyle(color: Colors.blue,fontSize: 20))),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'vertorneos', 
                          (route) => false
                        );
                      },
                       child:  const Text('Seleccionar',style: TextStyle(color: Colors.blue,fontSize: 20),))        
                ]
              ),
            
          ),
        ],
      ),
    );
  }
}