// ignore_for_file: camel_case_types
import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class Ajustes_Profile_User_Screen extends StatelessWidget {
   
  const Ajustes_Profile_User_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(),
      body: SizedBox(
        child: Column(
          children: const [
               _ajustesProfileUser() /*llamado a a los ajustes del perfil */
          ],
        ),
      )
    );
      }
    );
  }
}

//* AJUSTES DE PERFIL
class _ajustesProfileUser extends StatelessWidget {
  const _ajustesProfileUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context, orientation, deviceType) {
        return Column(
           children: [
           
             Form(child: 
             Padding(
               padding: EdgeInsets.only(top: 10.w, left: 5.w,right:5.w),
               child: Column(
                 children: 
                 [ 
  //*EDITAR FOTO DE PERFIL USUARIO
            Stack(
              children: [
                Padding(
                padding:  EdgeInsets.only(top: 5.w),
                child: CircleAvatar(
                  backgroundColor: Maintheme.tertiaryColor,
                radius: 15.w,
                    child: const FadeInImage(
                      fadeInCurve: Curves.decelerate,
                      placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
                    image:AssetImage('assets/imgs/backgrounds/perfil.png') //todo <== aqui debe mostrar la foto de perfil del manager
                    ),
                  ),
              ),
   //*EDITAR Y AGREGAR FOTO DE PERFIL USUARIO
              Padding(
                padding: EdgeInsets.only(top: 25.w,left: 20.w),
                child: IconButton(
                  onPressed: (){}, // todo <== debe agregar y seleccionar una foto para el perfil
                   icon: Icon(Icons.add_circle,color: Maintheme.secondaryColor,size: 10.w,),
                   )
              )
              ]
            ),
            Padding(
               padding: EdgeInsets.only(top: 3.w),
               child: Text('Edita elegir foto',style: Maintheme.fonstApp.subtitle1,),
             ), 
                      
    //* EDITAR NOMBRE USUARIO
                    Divider(height: 10.w,color: Maintheme.transparents,),
                      TextFormField(
                     autocorrect: false,
                     autofocus: false,
                     style: Maintheme.fonstApp.subtitle1,
                     keyboardType: TextInputType.emailAddress,
                       decoration: const InputDecoration(
                         hintText: 'Editar nombre',
                        
                       ),
                    ),
    //*CAMBIAR CORREO USUARIO
              Divider(height: 5.w,color: Maintheme.transparents,),
             TextFormField(
                     autocorrect: false,
                     autofocus: false,
                     style: Maintheme.fonstApp.subtitle1,
                     keyboardType: TextInputType.emailAddress,
                       decoration: const InputDecoration(
                         hintText: 'Editar correo',
                         
                       ),
                    ),
    //*CAMBIAR CONTRASEÑA USUARIO
             Divider(height: 5.w,color: Maintheme.transparents,),
             TextFormField(
                     autocorrect: false,
                     autofocus: false,
                     style: Maintheme.fonstApp.subtitle1,
                     keyboardType: TextInputType.emailAddress,
                       decoration: const InputDecoration(
                         hintText: 'Editar contraseña',
                       
                       ),
                    ),

    //* BOTON DE GUARDADO USUARIO
              Divider(height: 15.w,color: Maintheme.transparents,),
              SizedBox(
               height: 20.w,
               width: 70.w,
               child: ElevatedButton(
                 onPressed: (){}, // todo debe guardar los datos 
                 child: const Text('Guardar')
                 ),
                  )             
                 ],
               ),
             ))
           ],
         );
      }
    );
  }
}

