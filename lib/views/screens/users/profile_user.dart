// ignore_for_file: camel_case_types, unused_element

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

class PErfil_User_Screen extends StatelessWidget {
   
  const PErfil_User_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(),
      body: SizedBox(
        child: Column(
          children: const [
                _profileUser() /*llamado al perfil del usuario */
          ],
        ),
      )
    );
      }
     );
  }
}

//*AJUSTES DEL PERFIL DE USUARIO
class _profileUser extends StatelessWidget {
  const _profileUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
        return SizedBox(
        height: 150.w,
        child: Column(
          children: 
          [

    //*FOTO DE PERFIL DEL USUARIO       
          CircleAvatar(
            backgroundColor: Maintheme.tertiaryColor,
            radius: 15.w,
            child: const FadeInImage(
                fadeInCurve: Curves.decelerate,
                placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
              image:AssetImage('assets/imgs/backgrounds/perfil.png') //todo <== aqui debe mostrar la foto de perfil del USUARIO
               ),
             ),
              
    //*NOMBRE DEL USUARIO
           Divider(height: 3.w,),
           Column(
               children: [
                 Text('user.name',style: Maintheme.fonstApp.headline2,),//todo <== se debe hacer un llamado para el nombre del USUARIO
  //*CORREO DEL USUARIO
            Text('correo.user@gmail.com',style: Maintheme.fonstApp.headline5,), //todo <== se debe hacer un llamado para el nombre del USUARIO


  //* BOTON DE EDITAR PERFIL
             Padding(
               padding:  EdgeInsets.only(top: 4.w,left: 15.w,right: 15.w),
               child: SizedBox(
                height: 15.w,
                width: 15.h,
                 child: ElevatedButton(
                  onPressed: (){// todo <== debe llevar a edicion de perfil
                     Navigator.pushNamed(context, 'ajustesperfil');
                    },
                   child: const Text('Editar perfil')),
               ),
             )
               ],
             
           ), 

  //*AJUSTES DEL PERFIL TIPO TEXTO 
          Padding(
            padding:  EdgeInsets.only(left: 5.w,right: 5.w,top: 15.w),
            child: Column(
              children: [
  //* CENTRO DE AYUDA 
            Divider(height: 7.w,color: Maintheme.transparents,),  
                Row(
                  children: [
                 const Icon(Icons.support_agent_outlined,color: Maintheme.quaternaryColor,),   
                 Padding(
                   padding: EdgeInsets.only(left: 2.w),
                   child: GestureDetector(
                    onTap: (){Navigator.pushNamed(context, 'centroayuda');}, // todo <== debe llevar a Centro de ayuda
                    child: Text('Centro de ayuda',style: Maintheme.fonstApp.headline2,)),
                 )
                ],
              ),  

  //* TERMINOS Y CONDICIONES
            Divider(height: 7.w,color: Maintheme.transparents,),  
                Row(
                  children: [
                 const Icon(Icons.article_outlined,color: Maintheme.quaternaryColor,),   
                 Padding(
                   padding: EdgeInsets.only(left: 2.w),
                   child: GestureDetector(
                    onTap: (){Navigator.pushNamed(context, 'terminosycondiones');}, // todo <== debe llevar a Términos y condiciones
                    child: Text('Términos y condiciones',style: Maintheme.fonstApp.headline2,)),
                 )
                ],
              ), 

    //* CERRAR SESION
            Divider(height: 7.w,color: Maintheme.transparents,),  
                Row(
                  children: [
                 const Icon(Icons.exit_to_app_outlined,color: Maintheme.quaternaryColor,),   
                 Padding(
                   padding: EdgeInsets.only(left: 2.w),
                   child: GestureDetector(
                    onTap: (){cerrarSesion(context);}, // todo <== debe llevar Cerrar sesión
                    child: Text('Cerrar sesión',style: Maintheme.fonstApp.headline2,)),
                 )
                ],
              ),          

              ],
            ),
          )

          ],
        ),
       );
      }
     );
    
  }
  //*CUADRO DE DIALOGO PARA CERRAR SESION
    Future<String?> cerrarSesion(BuildContext context) {
    /**
     * Si el sistema operativo no es un iOS, entonces mostrará está alerta.
     * (Alertas para Android)
     */
    if (!Platform.isIOS) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          backgroundColor: Maintheme.tertiaryColor,
          elevation: 0,
          title: Text(
            'Deseas cerrar la sesión?',
            style:Maintheme.fonstApp.headline2,
          ),
          content: Text('Cerrarás la sesión pero podrás ingresar nuevamente',
              style: Maintheme.fonstApp.subtitle1,),
          actions: <Widget>[
            SizedBox(
              child: Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  Text('Cancelar',style: Maintheme.fonstApp.subtitle1,)),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'login', 
                          (route) => false
                        );
                      },
                       child:  Text('Aceptar',style: Maintheme.fonstApp.subtitle1,))        
                ]
              ),
            ),
          ],
        ),
      );
    }

    /**
     * Si el sistema operativo es un iOS, entonces mostrará está otra alerta.
     */
    return showCupertinoDialog(
      context: context,
      builder: (context) => CupertinoAlertDialog(
        title: Text(
          'Deseas cerrar la sesión?',
          style: Maintheme.fonstApp.subtitle1,
        ),
        content: Text('Cerrarás la sesión pero podrás ingresar nuevamente',
            style:Maintheme.fonstApp.subtitle1,),
        actions: <Widget>[
          CupertinoDialogAction(
            child:  Row(mainAxisAlignment: MainAxisAlignment.end,
                children: 
                [ 
                  TextButton(
                      onPressed: (){Navigator.of(context).pop();},
                       child:  const Text('Cancelar',style: TextStyle(color: Colors.blue,fontSize: 20))),
                  
                    TextButton(
                      onPressed: (){ //todo<== debe llevar al home de organizador
                         Navigator.pushNamedAndRemoveUntil(
                          context, 'login', 
                          (route) => false
                        );
                      },
                       child:  const Text('Aceptar',style: TextStyle(color: Colors.blue,fontSize: 20)))        
                ]
              ),
            
          ),
        ],
      ),
    );
  }
}