// ignore_for_file: unused_element

import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';

// ignore: camel_case_types
class Ver_Torneos_Screen extends StatelessWidget {
   
  const Ver_Torneos_Screen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
    return  Scaffold(
      appBar: AppBar(
        actions: [
      //*PERFIL DEL USUARIO
          Padding(
            padding:  EdgeInsets.only(right: 5.w),
            child: GestureDetector(
              onTap: (){ //todo <== debe llevar a los ajustes del perfil de usuario
                Navigator.pushNamed(context, 'perfiluser');
              },
              child: const CircleAvatar( // todo debe mostrar la foto del usuario
              backgroundColor: Maintheme.tertiaryColor,
              child: FadeInImage(
                 fadeInCurve: Curves.decelerate,
                placeholder:AssetImage('assets/imgs/backgrounds/loading.gif'), // todo <== imagen que se muestra antes de cargar
              image:AssetImage('assets/imgs/backgrounds/perfil.png') //todo <== aqui debe mostrar la foto de perfil del Users
              ),
              )
              ),
          )
        ],
      ),
      body:SizedBox(
        child: Column(
          children: 
          [
            Padding(
              padding:  EdgeInsets.only(top: 10.w),
              child: Text('Torneos',style: Maintheme.fonstApp.headline2,),
            ),

            const _listasTorneos(), /*llamado a la lista de torneo */
          ],
        ),
      )
    );
      }
     );
  }
}

//*LISTA DE TORNEOS
// ignore: camel_case_types
class _listasTorneos extends StatelessWidget {
  const _listasTorneos({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     return Sizer(
      builder: (context, orientation, deviceType) {
        return SizedBox(
           height: 145.w, //!posible error

     //* TORNEO #1      
          child: ListView(
            scrollDirection: Axis.vertical,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 5.w,right:5.w ),
                child: Card(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                  elevation:0,
                  color: Maintheme.tertiaryColor,
                  child: ListTile(
                    title: Text('Torneo 1',style: Maintheme.fonstApp.subtitle1,), //todo <== aqui debe ir el nombre delo de los torneos, creados por el organizador
                    leading: CircleAvatar( //todo <== aqui va la foto del torneo que el organizador haya subido 
                     backgroundColor: Maintheme.unselect2,
                     child: Text('Ftsas'.characters.first),//todo<== si no hay foto debe mostrar las primeras 2 o 3 letras del nombre del torneo
                    ),
                    onTap: (){Navigator.pushNamed(context, 'torneosiniciados');},//todo <== debe llevar a los partidos, posiciones, equipos del torneo de ese torneo
                  ),
                ),
              )
            ],

          ),
        );
      }
     );
  }
}