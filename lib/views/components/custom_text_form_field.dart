import 'package:flutter/material.dart';
import 'package:meet_new/theme/theme.dart';

class CustomTextFomrField extends StatelessWidget {
  final String? Function(String? value) validator;
  final void Function(String? value) onChanged;
  final String hintText;
  final bool isPsw;
  const CustomTextFomrField({
    Key? key,
    required this.validator,
    required this.onChanged,
    required this.hintText, this.isPsw = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autocorrect: false,
      autofocus: false,
      obscureText: isPsw,
      style: Maintheme.fonstApp.subtitle1,
      keyboardType: TextInputType.name,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        hintText: hintText,
      ),
      validator: (value) => validator(value),
      onChanged: (value) => onChanged(value),
    );
  }
}
