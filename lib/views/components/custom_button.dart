import 'package:flutter/material.dart';
import 'package:meet_new/models/models.dart';
import 'package:meet_new/theme/theme.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final void Function() onTap;
  const CustomButton({
    Key? key,
    required this.text,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return Container(
        height: 15.w,
        width: 90.w,
        decoration: BoxDecoration(
            gradient: Maintheme.gradiantStyle,
            borderRadius: BorderRadius.circular(20.w),
            boxShadow: [Maintheme.shadowBottons]),
        child: Center(
          child: GestureDetector(
            onTap: () {
              onTap();
            },
            child: Text(
              text,
              style: Maintheme.fonstApp.headline1,
            ),
          ),
        ),
      );
    });
  }
}
