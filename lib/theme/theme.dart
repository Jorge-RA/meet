

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
// ignore: unused_import
import 'package:meet_new/models/models.dart';


/*tema de la app 
colores
botones
animaciones
letras, etc
*/


class Maintheme{

  final hintexto = ''; /*texto de cada texformfields */


//*colores de la app */
static const primaryColor = Colors.white; /* utilizado para el fondo de la app y letras*/
static const secondaryColor = Color(0xff48CAE4); /* utilizado para botones y gradiante 1*/
static const tertiaryColor =Color(0xffEEEEEE);/*utilizado para card y widgest redondeados*/
static const quaternaryColor = Colors.black;/*color para letras */
static const transparents = Colors.transparent;/*utilizado para los divider, color transparente */
static const unselect = Color(0xff868685); /*color al deseccionar */
static const unselect2 = Color(0xffA0A0A0);
static const acepts = Color(0xff00D860); /*color verde para aceptaciones confirmadas */
static const noacepts = Color(0xffEF072D); /*color de botones para peticionnes de aceptacion denegada */
// ignore: use_full_hex_values_for_flutter_colors
static const gradiant2 = Color(0xfff72efdd); /*color del gradiante */


/*efecto gradiante para botones */
static const gradiantStyle =  LinearGradient(
  colors: 
  [
   secondaryColor,
   gradiant2,
  ],
   begin: Alignment.centerLeft,
   end:  Alignment.centerRight,
   
  );

  /*shadow de los botones */
 static  BoxShadow shadowBottons = BoxShadow(
       color: Colors.blue.withOpacity(0.2),
      spreadRadius: 4,
      blurRadius: 10,
      offset: const Offset(0, 3),
  );

 /*sombras para algunos botones */
  static Shadow shadows = const Shadow(
   color: Maintheme.secondaryColor,blurRadius: 10,

  );
//*letras de la app */
static const fonstApp = TextTheme(
  headline1: TextStyle(       /*tipografia usada en los titulos (color blanco, para botones azules) */
    fontFamily: "Heave",
     fontSize: 20,
     fontWeight: FontWeight.bold,
     color: primaryColor,
   ),
    headline2: TextStyle(     /*tipografia usada en los titulos (color negro, para botones fondos blancos) */
    fontFamily: "Heave",
     fontSize: 20,
    //  fontWeight: FontWeight.bold,
     color: quaternaryColor,
   ),
   subtitle1: TextStyle( /* tipografia usada para letras pequeñas, se usara en casi toda la app */
      fontFamily: 'Heave',
      fontSize: 15,
      fontWeight: FontWeight.w300,
      color: quaternaryColor,
   ),
    headline3: TextStyle(       /*tipografia usada en los titulos (color blanco, para botones azules, sin el fontWeight) */
    fontFamily: "Heave",
     fontSize: 20,
    //  fontWeight: FontWeight.bold,
     color: primaryColor,
   ),
    headline4: TextStyle(     /*tipografia usada en los titulos (color negro, para botones fondos blancos sin el fontWeight) */
    fontFamily: "Heave",
     fontSize: 20,
    //  fontWeight: FontWeight.bold,
     color: quaternaryColor,
   ), 
   headline5: TextStyle(
   fontFamily: "Heave",
   fontSize: 15,
   color: unselect2,
   ), 
    
    
);


//*tema de la app */

static final ThemeData lightTheme =  ThemeData.dark().copyWith(
   /*colores al hacer el scroll en android */
   visualDensity:  VisualDensity.adaptivePlatformDensity,
   backgroundColor: primaryColor,
   scaffoldBackgroundColor: primaryColor,
   colorScheme: ColorScheme.fromSwatch( /*color que se muestra al hacer scroll */
    accentColor:tertiaryColor
   ),
   
  /*app bar de todas las pantallas */
  primaryColor: quaternaryColor,
   appBarTheme: const AppBarTheme(
    iconTheme: IconThemeData(
      color: Maintheme.quaternaryColor
    ),
     centerTitle: true,
     backgroundColor: primaryColor,
     titleTextStyle: TextStyle(
       color:Maintheme.quaternaryColor,
       fontFamily: "Heave",
       fontSize: 15,
       fontWeight: FontWeight.w300
     ),
     elevation: 0,
   ),
   /*botones de navegacion, tipo button nav */
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
     elevation: 0,
     backgroundColor: tertiaryColor,
      selectedItemColor: quaternaryColor,  /* color al selecionar, al tocar un icono */   
      type:BottomNavigationBarType.fixed,  
   ),
   floatingActionButtonTheme: const FloatingActionButtonThemeData(
      backgroundColor: tertiaryColor,
      elevation: 0,
    ),
    /*animacion para Android y Ios */
      pageTransitionsTheme: const PageTransitionsTheme(
      builders: <TargetPlatform, PageTransitionsBuilder>{
        TargetPlatform.android: ZoomPageTransitionsBuilder(),
        TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
      },
    ),
     /*botones, asi se verán al crearlos */
     elevatedButtonTheme: ElevatedButtonThemeData(
      style: TextButton.styleFrom(
       primary: tertiaryColor,
        backgroundColor: secondaryColor,
           shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
           textStyle: const TextStyle(
             color: primaryColor, 
             fontWeight: FontWeight.bold,
              fontSize: 20,  
              fontFamily: "Heave"
          ),
      )
    ),
     /*texformfierld decoracion */
    inputDecorationTheme: InputDecorationTheme(
      filled: true,
      hintStyle: Maintheme.fonstApp.subtitle1,
     fillColor: Maintheme.tertiaryColor,
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
        borderSide: BorderSide.none,
      ), 
    ),
    /*text tipo boton */
    textButtonTheme: TextButtonThemeData(
       style: 
        ButtonStyle(
      textStyle: MaterialStateProperty.all(Maintheme.fonstApp.headline1),
      foregroundColor:  MaterialStateProperty.all(Maintheme.secondaryColor),
      overlayColor: MaterialStateProperty.all(Maintheme.primaryColor),
     shape: MaterialStateProperty.all(RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10))),
      
  )
 ),
  
  /* degradado de colores de boton */
  


 
);


}